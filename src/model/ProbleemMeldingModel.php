<?php
/**
 * Created by PhpStorm.
 * User: jessevandenberghe
 * Date: 15/10/2017
 * Time: 19:41
 */

namespace model;


class ProbleemMeldingModel implements \JsonSerializable
{
    private $id;
    private $locatieId;
    private $beschrijving;
    private $datum;
    private $afgerond;

    /**
     * ProbleemMeldingModel constructor.
     * @param $id
     * @param $locatieId
     * @param $beschrijving
     * @param $datum
     * @param $afgerond
     */
    public function __construct($id, $locatieId, $beschrijving, $datum, $afgerond)
    {
        $this->id = $id;
        $this->locatieId = $locatieId;
        $this->beschrijving = $beschrijving;
        $this->datum = $datum;
        $this->afgerond = $afgerond;
    }


    /**
     * @return mixed
     */
    public function getDatum()
    {
        return $this->datum;
    }

    /**
     * @param mixed $datum
     */
    public function setDatum($datum)
    {
        $this->datum = $datum;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getLocatieId()
    {
        return $this->locatieId;
    }

    /**
     * @param mixed $locatieId
     */
    public function setLocatieId($locatieId)
    {
        $this->locatieId = $locatieId;
    }

    /**
     * @return mixed
     */
    public function getBeschrijving()
    {
        return $this->beschrijving;
    }

    /**
     * @param mixed $beschrijving
     */
    public function setBeschrijving($beschrijving)
    {
        $this->beschrijving = $beschrijving;
    }

    /**
     * @return mixed
     */
    public function getAfgerond()
    {
        return $this->afgerond;
    }

    /**
     * @param mixed $afgerond
     */
    public function setAfgerond($afgerond)
    {
        $this->afgerond = $afgerond;
    }


    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}