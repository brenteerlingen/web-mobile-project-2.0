<?php
/**
 * Created by PhpStorm.
 * User: jessevandenberghe
 * Date: 15/10/2017
 * Time: 19:41
 */

namespace model;


class OpdrachtModel implements \JsonSerializable
{
    private $id;
    private $techniekerId;
    private $probleemMeldingId;

    /**
     * OpdrachtModel constructor.
     * @param $id
     * @param $techniekerId
     * @param $probleemMeldingId
     */
    public function __construct($id, $techniekerId, $probleemMeldingId)
    {
        $this->id = $id;
        $this->techniekerId = $techniekerId;
        $this->probleemMeldingId = $probleemMeldingId;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getTechniekerId()
    {
        return $this->techniekerId;
    }

    /**
     * @param mixed $techniekerId
     */
    public function setTechniekerId($techniekerId)
    {
        $this->techniekerId = $techniekerId;
    }

    /**
     * @return mixed
     */
    public function getProbleemMeldingId()
    {
        return $this->probleemMeldingId;
    }

    /**
     * @param mixed $probleemMeldingId
     */
    public function setProbleemMeldingId($probleemMeldingId)
    {
        $this->probleemMeldingId = $probleemMeldingId;
    }

    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}