<?php
/**
 * Created by PhpStorm.
 * User: brenteerlingen
 * Date: 5/10/17
 * Time: 08:53
 */

namespace model;


class StatusModel implements \JsonSerializable
{
    private $id;
    private $locatieid;
    private $status;
    private $datum;


    /**
     * StatusModel constructor.
     * @param $id
     * @param $locatieid
     * @param $status
     * @param $datum
     */


    public function __construct($id, $locatieid, $status, $datum)
    {
        $this->id = $id;
        $this->locatieid = $locatieid;
        $this->status = $status;
        $this->datum = $datum;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getLocatieid()
    {
        return $this->locatieid;
    }

    /**
     * @param mixed $locatieid
     */
    public function setLocatieid($locatieid)
    {
        $this->locatieid = $locatieid;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getDatum()
    {
        return $this->datum;
    }

    /**
     * @param mixed $datum
     */
    public function setDatum($datum)
    {
        $this->datum = $datum;
    }


    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    function jsonSerialize()
    {
        return get_object_vars($this);
    }
}
