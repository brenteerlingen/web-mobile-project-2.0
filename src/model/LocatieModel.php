<?php

namespace model;

class LocatieModel implements \JsonSerializable
{
    private $id;
    private $naam;

    public function __construct($id, $naam)
    {
        $this->setId($id);
        $this->setNaam($naam);
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getNaam()
    {
        return $this->naam;
    }

    public function setNaam($naam)
    {
        $this->naam = $naam;
    }
}