<?php
/**
 * Created by PhpStorm.
 * User: jessevandenberghe
 * Date: 15/10/2017
 * Time: 19:41
 */

namespace model;


class TechniekerModel implements \JsonSerializable
{
    private $id;
    private $naam;

    /**
     * TechniekerModel constructor.
     * @param $id
     * @param $naam
     */
    public function __construct($id, $naam)
    {
        $this->id = $id;
        $this->naam = $naam;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getNaam()
    {
        return $this->naam;
    }

    /**
     * @param mixed $naam
     */
    public function setNaam($naam)
    {
        $this->naam = $naam;
    }

    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}