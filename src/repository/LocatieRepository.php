<?php

namespace repository;
use model\LocatieModel;
interface LocatieRepository
{
     public function findLocations();
     public function findLocationById($id);

     public function addLocation(LocatieModel $location);
     public function updateLocation($location);
     public function deleteLocation($id);
}
