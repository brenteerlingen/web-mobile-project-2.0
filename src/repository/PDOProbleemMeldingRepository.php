<?php
/**
 * Created by PhpStorm.
 * User: jessevandenberghe
 * Date: 15/10/2017
 * Time: 19:48
 */

namespace repository;


use model\OpdrachtModel;
use model\ProbleemMeldingModel;


class PDOProbleemMeldingRepository implements ProbleemMeldingRepository
{

    private $connection = null;

    public function __construct(\PDO $connection)
    {
        $this->connection = $connection;
    }

    public function findProblems()
    {
        try {
            $statement = $this->connection->prepare('SELECT * FROM ProbleemMeldingen');
            $statement->execute();
            $results = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $arrayResults = array();

            if (count($results) > 0) {
                foreach ($results as $problem) {
                    array_push($arrayResults, new OpdrachtModel($problem['id'],$problem['locatieId'],$problem['omschrijving'],$problem['datum'],$problem['afgehandeld']));
                }
                return $arrayResults;
            } else {
                return null;
            }
        } catch (\Exception $exception) {
            return $exception->getMessage();
        }
    }

    public function findProblemById($id)
    {
        try{
            $statement = $this->connection->prepare('SELECT * FROM ProbleemMeldingen WHERE id = ?');
            $statement->bindParam(1,$id,\PDO::PARAM_INT);
            $statement->execute();
            $result = $statement->fetchAll(\PDO::FETCH_ASSOC);
            if(count($result) > 0){
                return new OpdrachtModel($result[0]['id'],$result[0]['locatieId'],$result[0]['omschrijving'],$result[0]['datum'],$result[0]['afgehandeld']);
            }
            else{
                return null;
            }
        }
        catch (\Exception $exception){
            return  $exception->getMessage();
        }
    }

    public function findCompletedProblems()
    {
        try {
            $statement = $this->connection->prepare('SELECT * FROM ProbleemMeldingen WHERE afgehandeld = 1');
            $statement->execute();
            $results = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $arrayResults = array();

            if (count($results) > 0) {
                foreach ($results as $problem) {
                    array_push($arrayResults, new OpdrachtModel($problem['id'],$problem['locatieId'],$problem['omschrijving'],$problem['datum'],$problem['afgehandeld']));
                }
                return $arrayResults;
            } else {
                return null;
            }
        } catch (\Exception $exception) {
            return $exception->getMessage();
        }
    }

    public function isCompleted($id)
    {
        try{
            $statement = $this->connection->prepare('SELECT * FROM ProbleemMeldingen WHERE id = ?');
            $statement->bindParam(1,$id,\PDO::PARAM_INT);
            $statement->execute();
            $result = $statement->fetchAll(\PDO::FETCH_ASSOC);
            if(count($result) > 0){
                return $result[0]['afgehandeld'];
            }
            else{
                return null;
            }
        }
        catch (\Exception $exception){
            return  $exception->getMessage();
        }
    }

    public function toggleCompleted($id)
    {
        try{
            if($this->isCompleted($id) == 1){
                $statement = $this->connection->prepare('UPDATE ProbleemMeldingen SET afgehandeld = 0 WHERE id = ?');
            }
            else{
                $statement = $this->connection->prepare('UPDATE ProbleemMeldingen SET afgehandeld = 1 WHERE id = ?');
            }
            $statement->bindParam(1,$id,\PDO::PARAM_INT);
            $statement->execute();

            return "De status afgehandeld is correct omgezet";
        }
        catch (\Exception $exception){
            $exception->getMessage();
        }
    }

    public function addProblem(ProbleemMeldingModel $problem)
    {
        try{
            $statement = $this->connection->prepare('INSERT INTO ProbleemMeldingen(locatieId, omschrijving, datum) VALUES (?,?,?)');

            $locatieId = $problem->getLocatieId();
            $beschrijving = str_replace('%20', ' ',$problem->getBeschrijving());
            $datum = str_replace('%20', ' ',$problem->getDatum());

            $statement->bindParam(1,$locatieId, \PDO::PARAM_INT);
            $statement->bindParam(2, $beschrijving,\PDO::PARAM_STR);
            $statement->bindParam(3, $datum, \PDO::PARAM_STR);
            $statement->execute();

            return "Probleem-melding succesvol toegevoegd.";
        }
        catch(\Exception $exception){
            $exception->getMessage();
        }
    }

    public function updateProblem(OpdrachtModel $problem)
    {
        try{
            $statement = $this->connection->prepare('UPDATE ProbleemMeldingen SET locatieId = ?, omschrijving = ?, datum = ?, afgehandeld = ? WHERE id = ?');
            $statement->bindParam(1,$problem['locatieId'], \PDO::PARAM_INT);
            $statement->bindParam(2, str_replace('%20', ' ',$problem['beschrijving']),\PDO::PARAM_STR);
            $statement->bindParam(3, str_replace('%20', ' ',$problem['datum']), \PDO::PARAM_STR);
            $statement->bindParam(4,$problem['afgerond'], \PDO::PARAM_INT);
            $statement->bindParam(5,$problem['id'], \PDO::PARAM_INT);
            $statement->execute();

            return "De status is correct omgezet";
        }
        catch(\Exception $exception){
            $exception->getMessage();
        }
    }

    public function deleteProblem($id)
    {
        try{
            $statement = $this->connection->prepare('DELETE FROM ProbleemMeldingen WHERE id = ?');
            $statement->bindParam(1,$id,\PDO::PARAM_INT);
            $statement->execute();

            return "Probleem-melding succesvol verwijderd";
        }
        catch (\Exception $exception){
            return  $exception->getMessage();
        }
    }
}