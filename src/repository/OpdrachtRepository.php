<?php

namespace repository;
interface OpdrachtRepository
{
     public function findOpdrachten();
     public function findOpdrachtById($id);

     public function assignOpdracht($techniekerId, $probleemmeldingId);
}
