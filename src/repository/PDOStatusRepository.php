<?php
/**
 * Created by PhpStorm.
 * User: brenteerlingen
 * Date: 5/10/17
 * Time: 08:44
 */

namespace repository;

use model\StatusModel;


class PDOStatusRepository implements StatusRepository
{
    private $connection = null;

    public function __construct(\PDO $connection)
    {
        $this->connection = $connection;
    }

   public function findAllStatusses(){
       try {
           $statement = $this->connection->prepare("SELECT * FROM StatusMeldingen");
           $statement->execute();
           $results = $statement->fetchAll(\PDO::FETCH_ASSOC);
           $arrayResults = array();
           if (count($results) > 0) {
               foreach ($results as $status) {
                   array_push($arrayResults, new StatusModel($status['id'], $status['locatieId'], $status['status'], $status['datum']));
               }
               return $arrayResults;
           } else {
               return null;
           }
       } catch (\Exception $exception) {
           return $exception->getMessage();
       }
    }


    public function findStatusById($id)
    {
        $statusId = $id;
        try {
            $statement = $this->connection->prepare("SELECT * FROM StatusMeldingen WHERE id = ?");
            $statement->bindParam(1, $statusId, \PDO::PARAM_INT);
            $statement->execute();
            $results = $statement->fetchAll(\PDO::FETCH_ASSOC);

            if (count($results) > 0) {
                return new StatusModel($results[0]['id'], $results[0]['locatieId'], $results[0]['status'], $results[0]['datum']);
            } else {
                return null;
            }
        } catch (\Exception $exception) {
            return $exception->getMessage();
        }
    }

    public function findStatusByLocatieId($id)
    {
        $locatieId = $id;
        try {
            $statement = $this->connection->prepare("SELECT * FROM StatusMeldingen WHERE locatieId = ?");
            $statement->bindParam(1, $locatieId, \PDO::PARAM_INT);
            $statement->execute();
            $results = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $arrayResults = array();

            if (count($results) > 0) {
                foreach ($results as $status) {
                    array_push($arrayResults, new StatusModel($status['id'], $status['locatieId'], $status['status'], $status['datum']));
                }
                return $arrayResults;
            } else {
                return null;
            }

        } catch (\Exception $exception) {
            return $exception->getMessage();
        }
    }

    public function findPopularLocations(){
        try {
            $statement = $this->connection->prepare("SELECT * FROM StatusMeldingen WHERE status = 'Goed' ORDER BY locatieId");
           // $statement->bindParam(1, , \PDO::PARAM_INT);
            $statement->execute();
            $results = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $arrayResults = array();

            if (count($results) > 0) {
                foreach ($results as $status) {
                        array_push($arrayResults, new StatusModel($status['id'], $status['locatieId'], $status['status'], $status['datum']));
                }
                return $arrayResults;
            } else {
                return null;
            }

        } catch (\Exception $exception) {
            return $exception->getMessage();
        }
    }

    public function addStatus(StatusModel $status)
    {
        try{
            $statement = $this->connection->prepare('INSERT INTO StatusMeldingen(locatieId, status, datum) VALUES (?,?,?)');

            $locatieId = $status->getLocatieid();
            $beschrijving = str_replace('%20', ' ',$status->getStatus());
            $datum = $status->getDatum();

            $statement->bindParam(1,$locatieId, \PDO::PARAM_INT);
            $statement->bindParam(2,$beschrijving,\PDO::PARAM_STR);
            $statement->bindParam(3,$datum, \PDO::PARAM_STR);
            $statement->execute();

            return "status succesvol toegevoegd.";
        }
        catch(\Exception $exception){
            $exception->getMessage();
        }
    }

    public function deleteStatusAfterHalfYear(){
        $today = date("Ymd", strtotime("-6 months"));

        $statement = $this->connection->prepare("SELECT * FROM StatusMeldingen");
        $statement->execute();
        $results = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $arrayResults = array();

        if (count($results) > 0) {
            foreach ($results as $status) {
                $statusDate = date("Ymd", strtotime($status['datum']));
                if($statusDate < $today) {
                    try {
                        $statement = $this->connection->prepare("DELETE FROM StatusMeldingen WHERE id = ?");
                        $statement->bindParam(1, $status['id'], \PDO::PARAM_INT);
                        $statement->execute();

                    } catch (\Exception $exception) {
                        return $exception->getMessage();
                    }
                } else {
                    array_push($arrayResults, new StatusModel($status['id'], $status['locatieId'], $status['status'], $status['datum']));
                }
            }
            return $arrayResults;
        } else {
            return null;
        }

    }

}