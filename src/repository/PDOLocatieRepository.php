<?php

namespace repository;

use model\LocatieModel;

class PDOLocatieRepository implements LocatieRepository
{
    private $connection = null;

    public function __construct(\PDO $connection)
    {
        $this->connection = $connection;
    }

    public function findLocations()
    {
        try {
            $statement = $this->connection->prepare('SELECT * FROM Locaties');
            $statement->execute();
            $results = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $arrayResults = array();

            if (count($results) > 0) {
                foreach ($results as $location) {
                    array_push($arrayResults, new LocatieModel($location['id'], $location['naam']));
                }
                return $arrayResults;
            } else {
                return null;
            }
        } catch (\Exception $exception) {
            return $exception->getMessage();
        }
    }

    public function findLocationById($id )
    {
        try {
            $statement = $this->connection->prepare("SELECT * FROM Locaties WHERE id = ?");
            $statement->bindParam(1, $id, \PDO::PARAM_INT);
            $statement->execute();
            $results = $statement->fetchAll(\PDO::FETCH_ASSOC);

            if (count($results) > 0) {
                return new LocatieModel($results[0]['id'], $results[0]['naam']);
            } else {
                return null;
            }
        } catch (\Exception $exception) {
            return $exception->getMessage();
        }
    }

    public function addLocation(LocatieModel $location)
    {
        $location->setNaam(str_replace('%20', ' ', $location->getNaam()));

        $locnaam = $location->getNaam();
        try {
            $statement = $this->connection->prepare('INSERT INTO Locaties (naam) VALUES (?)');
            $statement->bindParam(1, $locnaam, \PDO::PARAM_STR);
            $statement->execute();

            return 'Locatie is succesvol toegevoegd!';
        } catch (\Exception $exception) {
            return $exception->getMessage();
        }
    }

    public function updateLocation($location)
    {
        $location->setId(str_replace('%20', ' ', $location->getId()));
        $location->setNaam(str_replace('%20', ' ', $location->getNaam()));

        try {
            $statement = $this->connection->prepare('UPDATE Locaties SET naam = ? WHERE id = ?');
            $statement->bindParam(1, $location->getNaam(), \PDO::PARAM_STR);
            $statement->execute();

            return 'Locaties is succesvol gewijzigd!';
        } catch (\Exception $exception) {
            return $exception->getMessage();
        }

    }

    public function deleteLocation($id)
    {
        try {
            $statement = $this->connection->prepare('DELETE FROM Locaties WHERE id = ?');
            $statement->bindParam(1, $id, \PDO::PARAM_INT);
            $statement->execute();
            return 'Locatie is succesvol verwijderd!';
        } catch (\Exception $exception) {
            return $exception->getMessage();
        }
    }
}
