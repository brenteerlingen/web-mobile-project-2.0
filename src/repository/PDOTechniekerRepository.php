<?php
/**
 * Created by PhpStorm.
 * User: jessevandenberghe
 * Date: 15/10/2017
 * Time: 19:48
 */

namespace repository;


use model\TechniekerModel;

class PDOTechniekerRepository implements TechniekerRepostitory
{

    private $connection = null;

    public function __construct(\PDO $connection)
    {
        $this->connection = $connection;
    }


    public function findTechniekers()
    {
        try {
            $statement = $this->connection->prepare('SELECT * FROM Technieker');
            $statement->execute();
            $results = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $arrayResults = array();

            if (count($results) > 0) {
                foreach ($results as $technieker) {
                    array_push($arrayResults, new TechniekerModel($technieker['id'],$technieker['naam']));
                }
                return $arrayResults;
            } else {
                return null;
            }
        } catch (\Exception $exception) {
            return $exception->getMessage();
        }
    }

    public function findTechniekerById($id)
    {
        try{
            $statement = $this->connection->prepare('SELECT * FROM Technieker WHERE id = ?');
            $statement->bindParam(1,$id,\PDO::PARAM_INT);
            $statement->execute();
            $result = $statement->fetchAll(\PDO::FETCH_ASSOC);
            if(count($result) > 0){
                return new TechniekerModel($result[0]['id'],$result[0]['naam']);
            }
            else{
                return null;
            }
        }
        catch (\Exception $exception){
            return  $exception->getMessage();
        }
    }
}