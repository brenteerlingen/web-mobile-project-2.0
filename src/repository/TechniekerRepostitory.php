<?php

namespace repository;
interface TechniekerRepostitory
{
     public function findTechniekers();
     public function findTechniekerById($id);
}
