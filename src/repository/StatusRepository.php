<?php
/**
 * Created by PhpStorm.
 * User: brenteerlingen
 * Date: 5/10/17
 * Time: 08:44
 */

namespace repository;


use model\StatusModel;

interface StatusRepository
{
    public function findStatusById($id);
    public function findStatusByLocatieId($locatieId);
    public function findPopularLocations();
    public function deleteStatusAfterHalfYear();
    public function findAllStatusses();
    public function addStatus(StatusModel $status);
}