<?php
/**
 * Created by PhpStorm.
 * User: jessevandenberghe
 * Date: 15/10/2017
 * Time: 19:43
 */

namespace repository;


use model\OpdrachtModel;
use model\ProbleemMeldingModel;


interface ProbleemMeldingRepository
{
    public function findProblems();
    public function findProblemById($id);
    public function findCompletedProblems();
    public function isCompleted($id);
    public function toggleCompleted($id);
    public function addProblem(ProbleemMeldingModel $problem);
    public function updateProblem(OpdrachtModel $problem);
    public function deleteProblem($id);

}