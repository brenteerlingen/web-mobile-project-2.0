<?php
/**
 * Created by PhpStorm.
 * User: jessevandenberghe
 * Date: 15/10/2017
 * Time: 19:48
 */

namespace repository;


use model\OpdrachtModel;

class PDOOpdrachtRepository implements OpdrachtRepository
{

    private $connection = null;

    public function __construct(\PDO $connection)
    {
        $this->connection = $connection;
    }


    public function findOpdrachten()
    {
        try {
            $statement = $this->connection->prepare('SELECT * FROM Opdracht');
            $statement->execute();
            $results = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $arrayResults = array();

            if (count($results) > 0) {
                foreach ($results as $opdracht) {
                    array_push($arrayResults, new OpdrachtModel($opdracht['id'],$opdracht['techniekerId'],$opdracht['probleemMeldingId']));
                }
                return $arrayResults;
            } else {
                return null;
            }
        } catch (\Exception $exception) {
            return $exception->getMessage();
        }
    }

    public function findOpdrachtById($id)
    {
        try{
            $statement = $this->connection->prepare('SELECT * FROM Opdracht WHERE id = ?');
            $statement->bindParam(1,$id,\PDO::PARAM_INT);
            $statement->execute();
            $result = $statement->fetchAll(\PDO::FETCH_ASSOC);
            if(count($result) > 0){
                return new OpdrachtModel($result[0]['id'],$result[0]['techniekerId'],$result[0]['probleemMeldingId']);
            }
            else{
                return null;
            }
        }
        catch (\Exception $exception){
            return  $exception->getMessage();
        }
    }

    public function assignOpdracht($techniekerId, $probleemmeldingId)
    {
        try{
            $statement = $this->connection->prepare('INSERT INTO Opdracht(techniekerId, probleemMeldingId) VALUES (?,?)');
            $statement->bindParam(1,$techniekerId, \PDO::PARAM_INT);
            $statement->bindParam(2,$probleemmeldingId, \PDO::PARAM_INT);
            $statement->execute();

            return "Opdracht succesvol toegevoegd.";
        }
        catch(\Exception $exception){
            $exception->getMessage();
        }
    }
}