<?php
/**
 * Created by PhpStorm.
 * User: jessevandenberghe
 * Date: 15/10/2017
 * Time: 19:50
 */

namespace controller;

use repository\OpdrachtRepository;


class OpdrachtController
{
    private $opdrachtRepository;

    public function __construct(OpdrachtRepository $opdrachtRepository)
    {
        $this->opdrachtRepository = $opdrachtRepository;
    }

    public function handleFindOpdrachten(){
        $result = $this->opdrachtRepository->findOpdrachten();
        echo json_encode($result, JSON_PRETTY_PRINT);
    }
    public function handleFindOpdrachtById($id){
        $result = $this->opdrachtRepository->findOpdrachtById($id);
        echo json_encode($result, JSON_PRETTY_PRINT);
    }

    public function handleAssignOpdracht($techniekerId, $probleemMeldingId){
        $result = $this->opdrachtRepository->assignOpdracht($techniekerId,$probleemMeldingId);
        echo json_encode($result, JSON_PRETTY_PRINT);
    }

}