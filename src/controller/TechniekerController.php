<?php
/**
 * Created by PhpStorm.
 * User: jessevandenberghe
 * Date: 15/10/2017
 * Time: 19:50
 */

namespace controller;

use repository\TechniekerRepostitory;


class TechniekerController
{
    private $techniekerRepository;

    public function __construct(TechniekerRepostitory $techniekerRepostitory)
    {
        $this->techniekerRepository = $techniekerRepostitory;
    }

    public function handleFindTechniekers(){
        $result = $this->techniekerRepository->findTechniekers();
        echo json_encode($result, JSON_PRETTY_PRINT);
    }
    public function handleFindTechniekerById($id){
        $result = $this->techniekerRepository->findTechniekerById($id);
        echo json_encode($result, JSON_PRETTY_PRINT);
    }

}