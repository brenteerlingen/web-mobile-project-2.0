<?php

namespace controller;

use repository\LocatieRepository;

class LocatieController
{
    private $locatieRepository;

    public function __construct(LocatieRepository $locatieRepository)
    {
        $this->locatieRepository = $locatieRepository;
    }

    public function handleFindLocations()
    {
        $locatie = $this->locatieRepository->findLocations();
        echo json_encode($locatie, JSON_PRETTY_PRINT);
    }

    public function handleFindLocationById($id)
    {
        $locatie = $this->locatieRepository->findLocationById($id);
        echo json_encode($locatie, JSON_PRETTY_PRINT);
    }

    public function handleAddLocation($location) {
        $locatie = $this->locatieRepository->addLocation($location);
        echo json_encode($locatie, JSON_PRETTY_PRINT);
    }

    public function handleUpdateLocation($location) {
        $locatie = $this->locatieRepository->updateLocation($location);
        echo json_encode($locatie, JSON_PRETTY_PRINT);
    }

    public function handleDeleteLocation($id) {
        $locatie = $this->locatieRepository->deleteLocation($id);
        echo json_encode($locatie, JSON_PRETTY_PRINT);
    }
}
