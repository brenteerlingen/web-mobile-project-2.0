<?php
/**
 * Created by PhpStorm.
 * User: brenteerlingen
 * Date: 5/10/17
 * Time: 09:02
 */

namespace controller;


use repository\StatusRepository;
use model\StatusModel;

class StatusController
{
    private $statusRepo;

    public function __construct(StatusRepository $statusRepository)
    {
        $this->statusRepo = $statusRepository;
    }

    public function handleFindAllStatussen(){
        $status = $this->statusRepo->findAllStatusses();
        echo json_encode($status, JSON_PRETTY_PRINT);
    }

    public function handleFindStatusById($id)
    {
        $status = $this->statusRepo->findStatusById($id);
        echo json_encode($status,  JSON_PRETTY_PRINT);
    }

    public function handleFindStatusByLocationId($id){
        $status = $this->statusRepo->findStatusByLocatieId($id);
        echo json_encode($status, JSON_PRETTY_PRINT);
    }

    public function handleFindPopularStatusses(){
        $status = $this->statusRepo->findPopularLocations();
        echo json_encode($status, JSON_PRETTY_PRINT);
    }

    public function deleteOldStatusses(){
       $this->statusRepo->deleteStatusAfterHalfYear();
    }

    public function handleAddStatus(StatusModel $status){
        $result = $this->statusRepo->addStatus($status);
        echo json_encode($result, JSON_PRETTY_PRINT);
    }

}