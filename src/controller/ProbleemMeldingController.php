<?php
/**
 * Created by PhpStorm.
 * User: jessevandenberghe
 * Date: 15/10/2017
 * Time: 19:50
 */

namespace controller;

use model\OpdrachtModel;
use repository\ProbleemMeldingRepository;
use model\ProbleemMeldingModel;



class ProbleemMeldingController
{
    private $probleemMeldingRepository;

    /**
     * ProbleemMeldingController constructor.
     * @param $probleemMeldingRepository
     */
    public function __construct(ProbleemMeldingRepository $probleemMeldingRepository)
    {
        $this->probleemMeldingRepository = $probleemMeldingRepository;
    }

    public function handleFindProblems(){
        $result = $this->probleemMeldingRepository->findProblems();
        echo json_encode($result, JSON_PRETTY_PRINT);
    }
    public function handleFindProblemById($id){
        $result = $this->probleemMeldingRepository->findProblemById($id);
        echo json_encode($result, JSON_PRETTY_PRINT);
    }
    public function handleFindCompletedProblems(){
        $result = $this->probleemMeldingRepository->findCompletedProblems();
        echo json_encode($result, JSON_PRETTY_PRINT);
    }
    public function handleIsCompleted($id){
        $result = $this->probleemMeldingRepository->isCompleted($id);
        echo json_encode($result, JSON_PRETTY_PRINT);
    }
    public function handleToggleCompleted($id){
        $result = $this->probleemMeldingRepository->toggleCompleted($id);
        echo json_encode($result, JSON_PRETTY_PRINT);
    }
    public function handleAddProblem(ProbleemMeldingModel $problem){
        $result = $this->probleemMeldingRepository->addProblem($problem);
        echo json_encode($result, JSON_PRETTY_PRINT);
    }
    public function handleUpdateProblem(OpdrachtModel $problem){
        $result = $this->probleemMeldingRepository->updateProblem($problem);
        echo json_encode($result, JSON_PRETTY_PRINT);
    }
    public function handleDeleteProblem($id){
        $result = $this->probleemMeldingRepository->deleteProblem($id);
        echo json_encode($result, JSON_PRETTY_PRINT);
    }
}