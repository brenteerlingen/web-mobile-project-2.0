import React, {Component} from 'react';
import axios from 'axios';
import StatusTablePage from './statusTable'
import HttpService from '../common/http-service'
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import '../index.css';
import { connect } from "react-redux";
import mapDispatchToProps from '../common/title-dispatch-to-props';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ContentAdd from 'material-ui/svg-icons/content/add';
import { Link } from 'react-router-dom';

class StatusPage extends Component {
  constructor(){
    super();
    this.state = {entries: [], id: ""}
  }

  componentDidMount() {
      this.props.setTitle('StatusPage');
  }

  componentWillMount() {
    this.getAllStatusses();
  }

  getAllStatusses = () => {
    HttpService.getPopularStatusses().then(fetchedEntries => this.setState({
      entries: fetchedEntries
    }));
  }

  getPopularStatusses = () => {
    HttpService.getPopularStatusses().then(fetchedEntries => this.setState({
      entries: fetchedEntries
    }));
  }

  getStatusByLocationId = () => {
    const id = this.state.id;

    HttpService.getStatusByLocationId(id).then(fetchedEntries => this.setState({
      entries: fetchedEntries
    }));
  }

  deleteOldStatusses = () => {
    HttpService.deleteOldStatusses().then(fetchedEntries =>
      this.setState({
          entries: fetchedEntries
      }));
  }

  getAllStatusses = () => {
    HttpService.getAllStatusses().then(fetchedEntries => this.setState({
      entries: fetchedEntries
    }));
  }

  handleChange = (event) => {
    this.setState({
      id: event.target.value,
    });
  };


  render(){
    const fetchedEntries = this.state.entries || []
    return (
      <div>
          <RaisedButton onClick={this.getPopularStatusses} className="Button" label="Popular" primary={true}/>
          <RaisedButton onClick={this.deleteOldStatusses} className="Button" label="Delete old locations" primary={true}/>
          <RaisedButton onClick={this.getStatusByLocationId} className="Button" label="Location ID" primary={true}/>
          <TextField onChange={this.handleChange} hintText="Give location ID" type="number"/>
          <StatusTablePage entries={fetchedEntries}/>
            <Link to="/statussen/add">
                        <FloatingActionButton style={{ position: 'fixed', right: '15px', bottom: '15px' }}>
                            <ContentAdd />
                        </FloatingActionButton>
            </Link>
      </div>
    )
  }
}

export default connect(undefined, mapDispatchToProps)(StatusPage);
