import React, {Component} from 'react';
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';
import PropTypes from 'prop-types'
// import axios from 'axios';

class StatusTablePage extends Component {


  render(){
const rows = this.props.entries.map(e => (
  <TableRow key={e.id}>
    <TableRowColumn>{e.id}</TableRowColumn>
    <TableRowColumn>{e.locatieid}</TableRowColumn>
    <TableRowColumn>{e.status}</TableRowColumn>
    <TableRowColumn>{e.datum}</TableRowColumn>
  </TableRow>

));

    return (
      <Table>
      <TableHeader>
        <TableRow>
          <TableHeaderColumn>Id</TableHeaderColumn>
          <TableHeaderColumn>LocatieId</TableHeaderColumn>
          <TableHeaderColumn>Status</TableHeaderColumn>
          <TableHeaderColumn>Datum</TableHeaderColumn>
        </TableRow>
      </TableHeader>
      <TableBody>
          {rows}
      </TableBody>
    </Table>
    )
  }
}

StatusTablePage.propTypes = {

    entries: PropTypes.array.isRequired

}

export default StatusTablePage
