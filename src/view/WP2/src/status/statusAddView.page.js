import React, { Component } from 'react';
import { connect } from "react-redux";
import mapDispatchToProps from '../common/title-dispatch-to-props';
import TextField from 'material-ui/TextField';
import DatePicker from 'material-ui/DatePicker';
import FlatButton from 'material-ui/FlatButton';
import HttpService from '../common/http-service';
import { Link } from 'react-router-dom';
import moment from 'moment';

class StatusAddPage extends Component {
    constructor() {
        super();
        this.state = { showMessage: false, naam: ""};
    }
    render() {
        const message = (
            <div>
                <span>Status toegevoegd, klik <Link to='/statussen'>hier</Link> om terug te gaan.</span>
            </div>
        );
        return (
            <div>
                <form onSubmit={this.save}>
                    <TextField hintText="locatieId" name="locatieId" type="number" />
                    <TextField hintText="status" name="status" type="text" />
                    <DatePicker hintText="datum" name="date" />
                    <FlatButton label="Toevoegen" type="submit" />
                </form>
                {this.state.showMessage ? message : null}
            </div>
        );
    }

    handleChange = (event) => {
      this.setState({
        naam: event.target.value,
      });
    };

    save = (ev) => {
      ev.preventDefault();

      ev.preventDefault();
      const date = ev.target['date'].value;
      const status = ev.target['status'].value;
      const locatieId = ev.target['locatieId'].value;

      // date in juiste formaat YYYY-MM-dd => ddMMYYYY
      const momentDate = moment(date);
      const dateToSend = momentDate.format('DD/MM/YYYY');
      HttpService.addStatus(dateToSend, status, locatieId).then(() => {

          this.setState({ showMessage: true });
      });
    }
    componentDidMount() {
        this.props.setTitle('Toevoegen Locatie');
    }
}

export default connect(undefined, mapDispatchToProps)(StatusAddPage)
