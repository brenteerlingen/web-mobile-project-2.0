import React, { Component } from 'react';
import { connect } from "react-redux";
import mapDispatchToProps from '../common/title-dispatch-to-props';
import TextField from 'material-ui/TextField';
import DatePicker from 'material-ui/DatePicker';
import FlatButton from 'material-ui/FlatButton';
import HttpService from '../common/http-service';
import { Link } from 'react-router-dom';
import moment from 'moment';

class ProbleemMeldingAddPage extends Component {
    constructor() {
        super();
        this.state = { showMessage: false};
    }
    render() {
        const message = (
            <div>
                <span>Probleemmelding toegevoegd, klik <Link to='/problemen'>hier</Link> om terug te gaan.</span>
            </div>
        );
        return (
            <div>
                <form onSubmit={this.save}>
                    <TextField hintText="locatieId" name="locatieId" type="number" />
                    <TextField hintText="omschrijving" name="omschrijving" type="text" />
                    <DatePicker hintText="datum" name="date" />
                    <FlatButton label="Toevoegen" type="submit" />
                </form>
                {this.state.showMessage ? message : null}
            </div>
        );
    }
    save = (ev) => {
        ev.preventDefault();
        const date = ev.target['date'].value;
        const omschrijving = ev.target['omschrijving'].value;
        const locatieId = ev.target['locatieId'].value;

        // date in juiste formaat YYYY-MM-dd => ddMMYYYY
        const momentDate = moment(date);
        const dateToSend = momentDate.format('DD/MM/YYYY');
        HttpService.addProbleemMelding(dateToSend, omschrijving, locatieId).then(() => {

            this.setState({ showMessage: true });
        });
    }

    componentDidMount() {
        this.props.setTitle('Toevoegen ProbleemMelding');
    }
}

export default connect(undefined, mapDispatchToProps)(ProbleemMeldingAddPage)
