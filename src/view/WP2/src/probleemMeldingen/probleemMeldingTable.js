import React, {Component} from 'react';
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';

import PropTypes from 'prop-types'
// import axios from 'axios';

class ProbleemMeldingTablePage extends Component {


  render(){
const rows = this.props.entries.map(e => (
  <TableRow key={e.id}>
    <TableRowColumn>{e.id}</TableRowColumn>
    <TableRowColumn>{e.techniekerId}</TableRowColumn>
    <TableRowColumn>{e.probleemMeldingId}</TableRowColumn>

  </TableRow>

));

    return (
      <Table>
      <TableHeader>
        <TableRow>
          <TableHeaderColumn>Id</TableHeaderColumn>
          <TableHeaderColumn>TechniekerId</TableHeaderColumn>
          <TableHeaderColumn>ProbleemMeldingId</TableHeaderColumn>

        </TableRow>
      </TableHeader>
      <TableBody>
          {rows}
      </TableBody>
    </Table>
    )
  }
}

ProbleemMeldingTablePage.propTypes = {

    entries: PropTypes.array.isRequired

}

export default ProbleemMeldingTablePage
