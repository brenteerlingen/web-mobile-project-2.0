import React, {Component} from 'react';
import axios from 'axios';
import ProbleemMeldingTable from './probleemMeldingTable'
import HttpService from '../common/http-service'
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import '../index.css';
import { connect } from "react-redux";
import mapDispatchToProps from '../common/title-dispatch-to-props';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ContentAdd from 'material-ui/svg-icons/content/add';
import { Link } from 'react-router-dom';

class ProbleemMeldingPage extends Component {
  constructor(){
    super();
    this.state = {entries: [], id: ""}
  }

  componentDidMount() {
      this.props.setTitle('ProbleemMelding Page');
  }

  componentWillMount() {
    this.getAllProblems();
  }

  getAllProblems = () => {
    HttpService.getAllProbleemMeldingen().then(fetchedEntries => this.setState({
      entries: fetchedEntries
    }));
  }

  deleteProbleemMelding = () => {
    const id = this.state.id;

    HttpService.deleteProbleemMelding(id);

    this.getAllProblems();
  }

  getAfgerondeProbleemMeldingen = () => {
    HttpService.getAfgerondeProbleemMeldingen().then(fetchedEntries => this.setState({
      entries: fetchedEntries
    }));
  }

  handleChange = (event) => {
    this.setState({
      id: event.target.value,
    });
  };


  render(){
    const fetchedEntries = this.state.entries || []
    return (
      <div>
          <RaisedButton onClick={this.getAfgerondeProbleemMeldingen} className="Button" label="Afgerond" primary={true}/>
          <RaisedButton onClick={this.deleteProbleemMelding} className="Button" label="Delete probleemmelding met ID" primary={true}/>
          <TextField onChange={this.handleChange} hintText="Give probleem ID" type="number"/>
          <ProbleemMeldingTable entries={fetchedEntries} />
          <Link to="/problemen/add">
                      <FloatingActionButton style={{ position: 'fixed', right: '15px', bottom: '15px' }}>
                          <ContentAdd />
                      </FloatingActionButton>
          </Link>
      </div>
    )
  }
}

export default connect(undefined, mapDispatchToProps)(ProbleemMeldingPage);
