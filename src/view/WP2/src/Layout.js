import React, { Component } from 'react';
import AppBar from 'material-ui/AppBar';
import Drawer from 'material-ui/Drawer';
import MenuItem from 'material-ui/MenuItem';
import {
  BrowserRouter as  Router,
  Route,
  Link
} from 'react-router-dom'

import StatusPage from './status/statusView.page'
import LocatiesPage from './locaties/locatiesView.page'
import ProbleemMeldingPage from './probleemMeldingen/probleemMeldingView.page';
import ProbleemMeldingAddPage from './probleemMeldingen/problemenAddView.page';
import TechniekerPage from './techniekers/techniekerView.page';
import LocatieAddPage from './locaties/locatiesAddView.page';
import StatusAddPage from './status/statusAddView.page';
import { connect } from 'react-redux';

class Layout extends Component {

  constructor() {
    super();
    this.state = {drawerOpen: false};
  }

  toggleState = () => {
    const currentState = this.state.drawerOpen;
    this.setState({drawerOpen: !currentState});
  }

  render() {
    return(
    <Router>
    <div>
      <AppBar title={this.props.title} onLeftIconButtonTouchTap={this.toggleState}/>
      <Drawer open={this.state.drawerOpen}>
        <MenuItem onClick={this.toggleState}><Link to="/locaties">Locaties</Link></MenuItem>
        <MenuItem onClick={this.toggleState}><Link to="/statussen">Statussen</Link></MenuItem>
        <MenuItem onClick={this.toggleState}><Link to="/problemen">Probleem Meldingen</Link></MenuItem>
        <MenuItem onClick={this.toggleState}><Link to="/techniekers">Techniekers</Link></MenuItem>
    </Drawer>
    <Route exact={true} path="/locaties" component={LocatiesPage}/>
      <Route path="/locaties/add" component={LocatieAddPage} />
    <Route exact={true} path="/statussen" component={StatusPage}/>
      <Route path="/statussen/add" component={StatusAddPage}/>
    <Route exact={true} path="/problemen" component={ProbleemMeldingPage}/>
    <Route path="/problemen/add" component={ProbleemMeldingAddPage} />
    <Route exact={true} path="/techniekers" component={TechniekerPage}/>
    </div>
    </Router>
  );
  }
}



const mapStateToProps = (state, ownProps) => {
    return {
        title: state.title,
    }
}

export default connect(mapStateToProps)(Layout);
