import React, {Component} from 'react';
import axios from 'axios';
import HttpService from '../common/http-service'
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import '../index.css';
import { connect } from "react-redux";
import mapDispatchToProps from '../common/title-dispatch-to-props';
import { Link } from 'react-router-dom';
import TechniekerTablePage from "./techniekerTable";

class TechniekerPage extends Component {
    constructor(){
        super();
        this.state = {entries: [], id: ""}
    }

    componentDidMount() {
        this.props.setTitle('TechniekerPage');
    }

    componentWillMount() {
        this.getAllTechniekers();
    }

    getAllTechniekers = () => {
        HttpService.getAllTechniekers().then(fetchedEntries => this.setState({
            entries: fetchedEntries
        }));
    }

    render(){
        const fetchedEntries = this.state.entries || []
        return (
            <div>
                <TechniekerTablePage entries={fetchedEntries}/>
            </div>
        )
    }
}

export default connect(undefined, mapDispatchToProps)(TechniekerPage);
