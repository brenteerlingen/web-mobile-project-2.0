import React, {Component} from 'react';
import {
    Table,
    TableBody,
    TableHeader,
    TableHeaderColumn,
    TableRow,
    TableRowColumn,
} from 'material-ui/Table';

import PropTypes from 'prop-types'
// import axios from 'axios';

class TechniekerTablePage extends Component {


    render(){
        const rows = this.props.entries.map(e => (
            <TableRow key={e.id}>
                <TableRowColumn>{e.id}</TableRowColumn>
                <TableRowColumn>{e.naam}</TableRowColumn>

            </TableRow>

        ));

        return (
            <Table>
                <TableHeader>
                    <TableRow>
                        <TableHeaderColumn>Id</TableHeaderColumn>
                        <TableHeaderColumn>Naam</TableHeaderColumn>

                    </TableRow>
                </TableHeader>
                <TableBody>
                    {rows}
                </TableBody>
            </Table>
        )
    }
}

TechniekerTablePage.propTypes = {

    entries: PropTypes.array.isRequired

}

export default TechniekerTablePage
