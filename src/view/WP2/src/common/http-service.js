import axios from 'axios';

class HttpService {
    userId = null;
    baseURL = "http://192.168.33.11/web-mobile-project-2.0/api"
    testURL = "http://192.168.33.11/web-mobile-project-2.0/api/status/locaties/2"
  constructor(){

  }

  getAllLocaties(){
      return axios.get(`${this.baseURL}` + `/locaties`).then(r => r.data
      )
  }

  addLocatie(name) {

    let data = JSON.stringify({
        naam: name
    });

    return axios.post(`${this.baseURL}` + `/locaties/add`, data, {
        headers: {
            'Content-Type': 'application/json',
        }
    });
  }

  deleteLocatie(id) {
    return axios.delete(`${this.baseURL}` + `/locaties/delete/${id}`).then(r => r.data
    )
  }

  getAllStatusses(){
    return axios.get(`${this.baseURL}` + `/status`).then(r => r.data
    )
  }

  getStatusByLocationId(id){
    return axios.get(`${this.baseURL}` + `/status/location/${id}`).then(r => r.data
    )
  }

  getPopularStatusses(){
    return axios.get(`${this.baseURL}` + `/status/popular`).then(r => r.data
    )
  }

  deleteOldStatusses(){
    return axios.delete(`${this.baseURL}` + `/status/deleteold`).then(r => r.data
    )
  }

  addStatus(date, status, locatieId){
    return axios.post(`${this.baseURL}` + `/status/add`, { locatieId: locatieId, status: status, datum: date }
  );
  }

  getAllProbleemMeldingen(){
    return axios.get(`${this.baseURL}` + `/probleem`).then(r => r.data
    )
  }

  getAfgerondeProbleemMeldingen(){
    return axios.get(`${this.baseURL}` + `/probleem/afgerond`).then(r => r.data
    )
  }

  deleteProbleemMelding(id){
    return axios.delete(`${this.baseURL}` + `/probleem/delete/${id}`).then(r => r.data
    )
  }

  addProbleemMelding(date, omschrijving, locatieId) {


        return axios.post(`${this.baseURL}` + `/probleem/add`, { locatieId: locatieId, omschrijving: omschrijving, datum: date }
      );
  }

  getAllTechniekers(){
        return axios.get(`${this.baseURL}` + `/technieker`).then(r => r.data
        )
  }




}
const httpService = new HttpService()

export default httpService
