import React, { Component } from 'react';
import { connect } from "react-redux";
import mapDispatchToProps from '../common/title-dispatch-to-props';
import TextField from 'material-ui/TextField';
import FlatButton from 'material-ui/FlatButton';
import HttpService from '../common/http-service';
import { Link } from 'react-router-dom';

class LocatiesAddPage extends Component {
    constructor() {
        super();
        this.state = { showMessage: false, naam: ""};
    }
    render() {
        const message = (
            <div>
                <span>Locatie toegevoegd, klik <Link to='/locaties'>hier</Link> om terug te gaan.</span>
            </div>
        );
        return (
            <div>
                <form onSubmit={this.save}>
                    <TextField onChange={this.handleChange} hintText="naam" name="naam" type="string" />
                    <FlatButton label="Toevoegen" type="submit" />
                </form>
                {this.state.showMessage ? message : null}
            </div>
        );
    }

    handleChange = (event) => {
      this.setState({
        naam: event.target.value,
      });
    };

    save = (ev) => {
      ev.preventDefault();

      const name = this.state.naam;


      HttpService.addLocatie(name).then(() => {

          this.setState({ showMessage: true });
      });
    }
    componentDidMount() {
        this.props.setTitle('Toevoegen Locatie');
    }
}

export default connect(undefined, mapDispatchToProps)(LocatiesAddPage)
