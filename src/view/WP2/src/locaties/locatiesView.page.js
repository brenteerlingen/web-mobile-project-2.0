import React, {Component} from 'react';
import axios from 'axios';
import LocatieTablePage from './locatiesTable'
import HttpService from '../common/http-service'
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import '../index.css';
import { connect } from "react-redux";
import mapDispatchToProps from '../common/title-dispatch-to-props';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ContentAdd from 'material-ui/svg-icons/content/add';
import { Link } from 'react-router-dom';

class LocatiePage extends Component {
  constructor(){
    super();
    this.state = {entries: [], id: ""}
  }

  componentDidMount() {
    this.props.setTitle('Locaties Page');
  }

  componentWillMount() {
    this.getAllLocaties();
  }

  getAllLocaties = () => {
    HttpService.getAllLocaties().then(fetchedEntries => this.setState({
      entries: fetchedEntries
    }));
  }

  deleteLocatie = () => {
    const id = this.state.id;

    HttpService.deleteLocatie(id);

    this.getAllLocaties();
  }

  handleChange = (event) => {
    this.setState({
      id: event.target.value,
    });
  };

  render(){
    const fetchedEntries = this.state.entries
    return (
      <div>
          <RaisedButton onClick={this.deleteLocatie} className="Button" label="Verwijder locatie met ID" primary={true}/>
          <TextField onChange={this.handleChange} hintText="Geef locatie ID" type="number"/>
          <LocatieTablePage entries={fetchedEntries}/>
          <Link to="/locaties/add">
                      <FloatingActionButton style={{ position: 'fixed', right: '15px', bottom: '15px' }}>
                          <ContentAdd />
                      </FloatingActionButton>
          </Link>
      </div>
    )
  }
}

export default connect(undefined, mapDispatchToProps)(LocatiePage);
