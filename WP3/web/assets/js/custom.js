// Date picker
$('.datepicker').datepicker({
    autoclose: true,
    format: 'dd/mm/yyyy',
    todayHighlight: true,
    orientation: 'top',
    language: 'nl'
});

// Set modal
$('.deleteGebruiker').on('click', function() {
    $('#deleteGebruikerLink').attr("href", '/beheer/gebruikers/delete/' + $(this).attr('id'));
    $('#pearle-modal').modal();
});
$('.deleteStatus').on('click', function() {
    $('#deleteStatusLink').attr("href", '/status/delete/' + $(this).attr('id'));
    $('#pearle-modal').modal();
});
$('.deleteProbleem').on('click', function() {
    $('#deleteProbleemLink').attr("href", '/probleem/delete/' + $(this).attr('id'));
    $('#pearle-modal').modal();
});
$('.deleteLocatie').on('click', function() {
    $('#deleteLocatieLink').attr("href", '/locatie/delete/' + $(this).attr('id'));
    $('#pearle-modal').modal();
});