<?php

namespace WP3\Application\Controller\Beheer\Gebruiker;

use WP3\Domain\Model\Gebruiker;
use WP3\Domain\Repository\GebruikerRepositoryInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

class DeleteController
{
    private $gebruikerRepository;

    public function __construct(GebruikerRepositoryInterface $gebruikerRepository)
    {
        $this->gebruikerRepository = $gebruikerRepository;
    }

    public function __invoke(Gebruiker $gebruiker)
    {
        $this->gebruikerRepository->remove($gebruiker);

        return new RedirectResponse('/beheer');
    }
}
