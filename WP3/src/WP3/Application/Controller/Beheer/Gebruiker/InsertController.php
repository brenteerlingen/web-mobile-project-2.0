<?php

namespace WP3\Application\Controller\Beheer\Gebruiker;

use FOS\UserBundle\Doctrine\UserManager;
use WP3\Domain\Model\Gebruiker;
use WP3\Domain\Repository\GebruikerRepositoryInterface;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\BCryptPasswordEncoder;
use Symfony\Component\Security\Core\Encoder\EncoderFactory;
use Symfony\Component\Templating\EngineInterface;

class InsertController
{
    private $templating;
    private $form;
    private $encoderFactory;
    private $gebruikerRepository;

    public function __construct(EngineInterface $templating, GebruikerRepositoryInterface $gebruikerRepository, Form $form, EncoderFactory $encoderFactory)
    {
        $this->templating = $templating;
        $this->form = $form;
        $this->encoderFactory = $encoderFactory;
        $this->gebruikerRepository = $gebruikerRepository;
    }

    public function __invoke(Request $request)
    {
        $this->form->setData(new Gebruiker());
        $this->form->handleRequest($request);

        if ($this->form->isSubmitted() && $this->form->isValid()) {
            $gebruiker = $this->form->getData();

            $encoder = $this->encoderFactory->getEncoder($gebruiker);

            if ($encoder instanceof BCryptPasswordEncoder) {
                $gebruiker->setSalt(null);
            } else {
                $salt = rtrim(str_replace('+', '.', base64_encode(random_bytes(32))), '=');
                $gebruiker->setSalt($salt);
            }

            $gebruiker->setPassword($hashedPassword = $encoder->encodePassword($gebruiker->getPassword(), $gebruiker->getSalt()));
            $gebruiker->setEnabled(true);

            $this->gebruikerRepository->persist($gebruiker);

            return new RedirectResponse('/beheer');
        }

        return $this->templating->renderResponse(
            'Beheer/Gebruiker/insert.html.twig',
            [
                "form" => $this->form->createView()
            ]
        );
    }
}
