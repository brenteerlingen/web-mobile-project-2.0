<?php

namespace WP3\Application\Controller\Beheer\Gebruiker;

use WP3\Domain\Model\Gebruiker;
use WP3\Domain\Repository\GebruikerRepositoryInterface;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;
use Symfony\Component\Templating\EngineInterface;

class UpdateController
{
    private $form;
    private $templating;
    private $encoderFactory;
    private $gebruikerRepository;

    public function __construct(EngineInterface $templating, GebruikerRepositoryInterface $gebruikerRepository, Form $form, EncoderFactoryInterface $encoderFactory)
    {
        $this->templating = $templating;
        $this->form = $form;
        $this->encoderFactory = $encoderFactory;
        $this->gebruikerRepository = $gebruikerRepository;
    }

    public function __invoke(Request $request, Gebruiker $gebruiker)
    {
        $this->form->setData($gebruiker);
        $this->form->handleRequest($request);

        if ($this->form->isSubmitted() && $this->form->isValid()) {
            $gebruiker = $this->form->getData();

            $encoder = $this->encoderFactory->getEncoder($gebruiker);
            $gebruiker->setPassword($encoder->encodePassword($gebruiker->getPassword(), $gebruiker->getSalt()));

            $this->gebruikerRepository->persist($gebruiker);

            return new RedirectResponse('/beheer');
        }

        return $this->templating->renderResponse(
            'Beheer/Gebruiker/edit.html.twig',
            [
                "form" => $this->form->createView()
            ]
        );
    }
}
