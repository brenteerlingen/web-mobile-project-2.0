<?php

namespace WP3\Application\Controller\Beheer\Gebruiker;

use WP3\Domain\Repository\GebruikerRepositoryInterface;
use Symfony\Component\HttpFoundation\Response;

class GetController
{
    private $gebruikerRepository;

    public function __construct(GebruikerRepositoryInterface $gebruikerRepository)
    {
        $this->gebruikerRepository = $gebruikerRepository;
    }

    public function __invoke()
    {
        $jsonGebruikers = [];
        $gebruikers = $this->gebruikerRepository->findAllQuery()->getQuery()->getResult();

        foreach ($gebruikers as $gebruiker) {
            $json = $gebruiker->__toJson();
            array_push($jsonGebruikers, $json);
        }

        return new Response(
            json_encode($jsonGebruikers)
        );
    }
}
