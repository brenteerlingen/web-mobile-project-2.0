<?php

namespace WP3\Application\Controller\Beheer;

use WP3\Application\Pager\Paginator;
use WP3\Domain\Repository\GebruikerRepositoryInterface;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Templating\EngineInterface;

class IndexController
{
    private $templating;
    private $gebruikerRepository;
    private $paginator;
    private $form;

    public function __construct(EngineInterface $templating, GebruikerRepositoryInterface $gebruikerRepository, Paginator $paginator, Form $form)
    {
        $this->templating = $templating;
        $this->gebruikerRepository = $gebruikerRepository;
        $this->paginator = $paginator;
        $this->form = $form;
    }

    public function __invoke(Request $request)
    {
        $gebruikers = $this->paginator->create($this->gebruikerRepository->findAllQuery(), $request, 'g.username', 'asc', 10);
        $this->form->handleRequest($request);

        return $this->templating->renderResponse(
            'Beheer/index.html.twig', ['gebruikers' => $gebruikers,
                'form' => $this->form->createView()]
        );
    }
}
