<?php

namespace WP3\Application\Controller\Status;

use WP3\Application\Pager\Paginator;
use WP3\Domain\Repository\StatusRepositoryInterface;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Templating\EngineInterface;

class IndexController
{
    private $templating;
    private $statusRepository;
    private $paginator;
    private $form;

    public function __construct(EngineInterface $templating, StatusRepositoryInterface $statusRepository, Paginator $paginator, Form $form)
    {
        $this->templating = $templating;
        $this->statusRepository = $statusRepository;
        $this->paginator = $paginator;
        $this->form = $form;
    }

    public function __invoke(Request $request)
    {
        $statussen = $this->paginator->create($this->statusRepository->findAllQuery(), $request, 's.datum', 'desc', 10);
        $this->form->handleRequest($request);

        return $this->templating->renderResponse(
            'Status/index.html.twig', ['statussen' => $statussen,
                'form' => $this->form->createView()]
        );
    }
}
