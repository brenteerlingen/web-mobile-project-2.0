<?php

namespace WP3\Application\Controller\Status;

use WP3\Domain\Model\Status;
use WP3\Domain\Repository\StatusRepositoryInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

class DeleteController
{
    private $statusRepository;

    public function __construct(StatusRepositoryInterface $statusRepository)
    {
        $this->statusRepository = $statusRepository;
    }

    public function __invoke(Status $status)
    {
        $this->statusRepository->remove($status);

        return new RedirectResponse('/status');
    }
}
