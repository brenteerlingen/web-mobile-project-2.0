<?php

namespace WP3\Application\Controller\Status;

use WP3\Domain\Model\Status;
use WP3\Domain\Repository\StatusRepositoryInterface;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Templating\EngineInterface;

class UpdateController
{
    private $templating;
    private $statusRepository;
    private $form;

    public function __construct(EngineInterface $templating, StatusRepositoryInterface $statusRepository, Form $form)
    {
        $this->templating = $templating;
        $this->statusRepository = $statusRepository;
        $this->form = $form;
    }

    public function __invoke(Request $request, Status $status)
    {
        $this->form->setData($status);
        $this->form->handleRequest($request);

        if ($this->form->isSubmitted() && $this->form->isValid()) {
            $status = $this->form->getData();

            $status->setDatum(new \DateTime());

            $this->statusRepository->persist($status);

            return new RedirectResponse('/status');
        }

        return $this->templating->renderResponse(
            'Status/edit.html.twig',
            [
                "form" => $this->form->createView()
            ]
        );
    }
}
