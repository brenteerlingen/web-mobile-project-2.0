<?php

namespace WP3\Application\Controller\Probleem;

use WP3\Domain\Model\Probleem;
use WP3\Domain\Repository\ProbleemRepositoryInterface;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Templating\EngineInterface;

class InsertController
{
    private $templating;
    private $form;
    private $probleemRepository;

    public function __construct(EngineInterface $templating, ProbleemRepositoryInterface $probleemRepository, Form $form)
    {
        $this->templating = $templating;
        $this->form = $form;
        $this->probleemRepository = $probleemRepository;
    }

    public function __invoke(Request $request)
    {
        $this->form->setData(new Probleem());
        $this->form->handleRequest($request);

        if ($this->form->isSubmitted() && $this->form->isValid()) {
            $probleem = $this->form->getData();

            $probleem->setDatum(new \DateTime());

            $this->probleemRepository->persist($probleem);

            return new RedirectResponse('/probleem');
        }

        return $this->templating->renderResponse(
            'Probleem/insert.html.twig',
            [
                "form" => $this->form->createView()
            ]
        );
    }
}
