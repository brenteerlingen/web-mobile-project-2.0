<?php

namespace WP3\Application\Controller\Probleem;

use WP3\Domain\Model\Probleem;
use WP3\Domain\Repository\ProbleemRepositoryInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

class DeleteController
{
    private $probleemRepository;

    public function __construct(ProbleemRepositoryInterface $probleemRepository)
    {
        $this->probleemRepository = $probleemRepository;
    }

    public function __invoke(Probleem $probleem)
    {
        $this->probleemRepository->remove($probleem);

        return new RedirectResponse('/probleem');
    }
}
