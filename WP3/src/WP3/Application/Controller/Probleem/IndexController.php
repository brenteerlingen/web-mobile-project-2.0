<?php

namespace WP3\Application\Controller\Probleem;

use WP3\Application\Pager\Paginator;
use WP3\Domain\Repository\ProbleemRepositoryInterface;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Templating\EngineInterface;

class IndexController
{
    private $templating;
    private $probleemRepository;
    private $paginator;
    private $form;

    public function __construct(EngineInterface $templating, ProbleemRepositoryInterface $probleemRepository, Paginator $paginator, Form $form)
    {
        $this->templating = $templating;
        $this->probleemRepository = $probleemRepository;
        $this->paginator = $paginator;
        $this->form = $form;
    }

    public function __invoke(Request $request)
    {
        $probleemmeldingen = $this->paginator->create($this->probleemRepository->findAllQuery(), $request, 'p.datum', 'desc', 10);
        $this->form->handleRequest($request);

        return $this->templating->renderResponse(
            'Probleem/index.html.twig', ['probleemmeldingen' => $probleemmeldingen,
                'form' => $this->form->createView()]
        );
    }
}
