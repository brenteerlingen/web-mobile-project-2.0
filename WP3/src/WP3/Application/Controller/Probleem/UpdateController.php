<?php

namespace WP3\Application\Controller\Probleem;

use WP3\Domain\Model\Probleem;
use WP3\Domain\Repository\ProbleemRepositoryInterface;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Templating\EngineInterface;

class UpdateController
{
    private $templating;
    private $probleemRepository;
    private $form;

    public function __construct(EngineInterface $templating, ProbleemRepositoryInterface $probleemRepository, Form $form)
    {
        $this->templating = $templating;
        $this->probleemRepository = $probleemRepository;
        $this->form = $form;
    }

    public function __invoke(Request $request, Probleem $probleem)
    {
        $this->form->setData($probleem);
        $this->form->handleRequest($request);

        if ($this->form->isSubmitted() && $this->form->isValid()) {
            $probleem = $this->form->getData();

            $probleem->setDatum(new \DateTime());

            $this->probleemRepository->persist($probleem);

            return new RedirectResponse('/probleem');
        }

        return $this->templating->renderResponse(
            'Probleem/edit.html.twig',
            [
                "form" => $this->form->createView()
            ]
        );
    }
}
