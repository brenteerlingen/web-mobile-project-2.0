<?php

namespace WP3\Application\Controller\ProbleemToegekend;

use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use WP3\Application\Pager\Paginator;
use WP3\Domain\Repository\ProbleemRepositoryInterface;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Templating\EngineInterface;

class IndexController
{
    private $templating;
    private $probleemRepository;
    private $paginator;
    private $form;

    private $user;
    private $tokenStorage;

    public function __construct(EngineInterface $templating, ProbleemRepositoryInterface $probleemRepository, Paginator $paginator, Form $form, TokenStorageInterface $tokenStorage)
    {
        $this->templating = $templating;
        $this->probleemRepository = $probleemRepository;
        $this->paginator = $paginator;
        $this->form = $form;
        $this->tokenStorage = $tokenStorage;

        $this->user = $this->tokenStorage->getToken()->getUser();
    }

    public function __invoke(Request $request)
    {


        $probleemmeldingen = $this->paginator->create($this->probleemRepository->findAllByTechnicus($this->user->getUsername()), $request, 'p.datum', 'desc', 10);
        $this->form->handleRequest($request);

        return $this->templating->renderResponse(
            'ProbleemToegekend/index.html.twig', ['probleemmeldingen' => $probleemmeldingen,
                'form' => $this->form->createView()]
        );
    }
}
