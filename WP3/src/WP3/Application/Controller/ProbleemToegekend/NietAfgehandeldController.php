<?php

namespace WP3\Application\Controller\ProbleemToegekend;

use WP3\Domain\Model\Probleem;
use WP3\Domain\Repository\ProbleemRepositoryInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

class NietAfgehandeldController
{
    private $probleemRepository;

    public function __construct(ProbleemRepositoryInterface $probleemRepository)
    {
        $this->probleemRepository = $probleemRepository;
    }

    public function __invoke(Request $request, Probleem $probleem)
    {
        $probleem->setAfgehandeld(0);

        $this->probleemRepository->persist($probleem);

        return new RedirectResponse('/toegekendeproblemen');
    }
}
