<?php

namespace WP3\Application\Controller\Locatie;

use WP3\Domain\Model\Locatie;
use WP3\Domain\Repository\LocatieRepositoryInterface;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Templating\EngineInterface;

class UpdateController
{
    private $templating;
    private $locatieRepository;
    private $form;

    public function __construct(EngineInterface $templating, LocatieRepositoryInterface $locatieRepository, Form $form)
    {
        $this->templating = $templating;
        $this->locatieRepository = $locatieRepository;
        $this->form = $form;
    }

    public function __invoke(Request $request, Locatie $locatie)
    {
        $this->form->setData($status);
        $this->form->handleRequest($request);

        if ($this->form->isSubmitted() && $this->form->isValid()) {
            $locatie = $this->form->getData();

            $this->locatieRepository->persist($locatie);

            return new RedirectResponse('/locatie');
        }

        return $this->templating->renderResponse(
            'Locatie/edit.html.twig',
            [
                "form" => $this->form->createView()
            ]
        );
    }
}
