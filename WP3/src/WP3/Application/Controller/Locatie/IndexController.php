<?php

namespace WP3\Application\Controller\Locatie;

use WP3\Application\Pager\Paginator;
use WP3\Domain\Repository\LocatieRepositoryInterface;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Templating\EngineInterface;

class IndexController
{
    private $templating;
    private $paginator;
    private $form;
    private $locatieRepository;

    public function __construct(EngineInterface $templating, LocatieRepositoryInterface $locatieRepository, Paginator $paginator, Form $form)
    {
        $this->templating = $templating;
        $this->paginator = $paginator;
        $this->form = $form;
        $this->locatieRepository = $locatieRepository;
    }

    public function __invoke(Request $request)
    {
        $locaties = $this->paginator->create($this->locatieRepository->findAllQuery(), $request);
        $this->form->handleRequest($request);

        return $this->templating->renderResponse(
            'Locatie/index.html.twig', ['locaties' => $locaties,
                'form' => $this->form->createView()]
        );
    }
}
