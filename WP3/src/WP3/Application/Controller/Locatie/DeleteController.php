<?php

namespace WP3\Application\Controller\Locatie;

use WP3\Domain\Model\Locatie;
use WP3\Domain\Repository\LocatieRepositoryInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

class DeleteController
{
    private $locatieRepository;

    public function __construct(LocatieRepositoryInterface $locatieRepository)
    {
        $this->locatieRepository = $locatieRepository;
    }

    public function __invoke(Locatie $locatie)
    {
        $this->locatieRepository->remove($locatie);

        return new RedirectResponse('/locatie');
    }
}
