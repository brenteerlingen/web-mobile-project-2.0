<?php

namespace WP3\Application\Form;

use WP3\Domain\Model\Status;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class StatusType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Status::class
        ]);
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('locatieId', TextType::class, [
                'label' => 'Locatie ID',
                'required' => true,
                'attr' => [
                    'placeholder' => 'Locatie ID...']
            ])
            ->add('status', TextType::class, [
                'label' => 'Status',
                'required' => true,
                'attr' => [
                    'placeholder' => 'Status...']
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Opslaan',
                'attr'   => [
                    'class'   => 'btn-success']
            ]);
    }
}
