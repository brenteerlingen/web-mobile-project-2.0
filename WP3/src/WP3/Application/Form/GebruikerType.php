<?php

namespace WP3\Application\Form;

use WP3\Domain\Model\Gebruiker;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class GebruikerType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Gebruiker::class
        ]);
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', TextType::class, [
                'label' => 'Gebruikersnaam',
                'required' => true,
                'attr' => [
                    'placeholder' => 'Gebruikersnaam']
            ])
            ->add('password', PasswordType::class, [
                'label' => 'Wachtwoord',
                'required' => true,
                'attr' => [
                    'placeholder' => 'Wachtwoord']
            ])
            ->add('roles', ChoiceType::class, [
                'label' => 'Rol',
                'required' => true,
                'multiple' => true,
                'choices' => [
                    'Werkbeheer' => 'ROLE_WERKBEHEERDER',
                    'Technicus' => 'ROLE_TECHNICUS',
                    'Administrator' => 'ROLE_ADMIN'
                ]
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Opslaan',
                'attr'   => [
                    'class'   => 'btn-success']
            ]);
    }
}
