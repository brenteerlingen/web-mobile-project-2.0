<?php

namespace WP3\Application\Form;

use WP3\Domain\Model\Probleem;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use WP3\Domain\Repository\GebruikerRepositoryInterface;

class ProbleemType extends AbstractType
{
//    private $gebruikerRepository;

//    public function __construct(GebruikerRepositoryInterface $gebruikerRepository)
//    {
//        $this->gebruikerRepository = $gebruikerRepository;
//    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Probleem::class
        ]);
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('locatieId', TextType::class, [
                'label' => 'Locatie ID',
                'required' => true,
                'attr' => [
                    'placeholder' => 'Locatie ID...']
            ])
            ->add('omschrijving', TextType::class, [
                'label' => 'Omschrijving',
                'required' => true,
                'attr' => [
                    'placeholder' => 'Omschrijving...']
            ])
            ->add('technicus', EntityType::class, [
                'class' => 'WP3\Domain\Model\Gebruiker',
                'required' => true,
                'choice_label' => function ($gebruiker) {
                    return $gebruiker->getUsername();
                },
                'attr' => [
                    'class' => 'md-form-control md-valid']
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Opslaan',
                'attr'   => [
                    'class'   => 'btn-success']
            ]);
    }
}
