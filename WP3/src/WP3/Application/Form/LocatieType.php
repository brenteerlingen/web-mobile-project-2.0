<?php

namespace WP3\Application\Form;

use WP3\Domain\Model\Locatie;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LocatieType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Locatie::class
        ]);
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('naam', TextType::class, [
                'label' => 'Naam',
                'required' => true,
                'attr' => [
                    'placeholder' => 'Naam...']
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Opslaan',
                'attr'   => [
                    'class'   => 'btn-success']
            ]);
    }
}
