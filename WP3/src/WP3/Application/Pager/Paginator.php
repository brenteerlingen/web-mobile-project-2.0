<?php

namespace WP3\Application\Pager;

use Symfony\Component\HttpFoundation\Request;
use Knp\Component\Pager\Paginator as BasePaginator;

class Paginator
{
    private $paginator;

    public function __construct(BasePaginator $paginator)
    {
        $this->paginator = $paginator;
    }

    public function create($query, Request $request, $sortName = null, $sortDir = null, $perPage = 5)
    {
        return $this->paginator->paginate(
            $query,
            $request->query->getInt('page', 1),
            $perPage,
            array('defaultSortFieldName' => $sortName, 'defaultSortDirection' => $sortDir)
        );
    }
}
