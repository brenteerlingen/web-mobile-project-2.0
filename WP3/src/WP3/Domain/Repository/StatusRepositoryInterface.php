<?php

namespace WP3\Domain\Repository;

interface StatusRepositoryInterface
{
    public function findOneById($id);
    public function findAllQuery();
    public function persist($status);
    public function remove($status);
}
