<?php

namespace WP3\Domain\Repository;

interface LocatieRepositoryInterface
{
    public function findOneById($id);
    public function findAllQuery();
    public function persist($locatie);
    public function remove($locatie);
}
