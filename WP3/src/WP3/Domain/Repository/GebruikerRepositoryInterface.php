<?php

namespace WP3\Domain\Repository;

interface GebruikerRepositoryInterface
{
    public function findOneById($id);
    public function findAllQuery();
    public function findByTechnicus();
    public function persist($gebruiker);
    public function remove($gebruiker);
}