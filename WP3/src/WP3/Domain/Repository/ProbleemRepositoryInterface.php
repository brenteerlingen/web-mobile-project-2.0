<?php

namespace WP3\Domain\Repository;

interface ProbleemRepositoryInterface
{
    public function findOneById($id);
    public function findAllQuery();
    public function findAllByTechnicus($id);
    public function persist($probleem);
    public function remove($probleem);
}
