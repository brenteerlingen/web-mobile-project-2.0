<?php

namespace WP3\Domain\Model;

class Probleem
{
    private $id;
    private $locatieId;
    private $omschrijving;
    private $datum;
    private $afgehandeld = 0;
    private $technicus;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getLocatieId()
    {
        return $this->locatieId;
    }

    /**
     * @param mixed $locatieId
     */
    public function setLocatieId($locatieId)
    {
        $this->locatieId = $locatieId;
    }

    /**
     * @return mixed
     */
    public function getOmschrijving()
    {
        return $this->omschrijving;
    }

    /**
     * @param mixed $omschrijving
     */
    public function setOmschrijving($omschrijving)
    {
        $this->omschrijving = $omschrijving;
    }

    /**
     * @return mixed
     */
    public function getDatum()
    {
        return $this->datum;
    }

    /**
     * @param mixed $datum
     */
    public function setDatum($datum)
    {
        $this->datum = $datum;
    }

    /**
     * @return mixed
     */
    public function getAfgehandeld()
    {
        return (boolean)$this->afgehandeld;
    }

    /**
     * @param mixed $afgehandeld
     */
    public function setAfgehandeld($afgehandeld)
    {
        $this->afgehandeld = $afgehandeld;
    }

    /**
     * @return mixed
     */
    public function getTechnicus()
    {
        return $this->technicus;
    }

    /**
     * @param mixed $technicus
     */
    public function setTechnicus($technicus)
    {
        $this->technicus = $technicus;
    }

    public function toString() {
        return $this->technicus;
    }
}
