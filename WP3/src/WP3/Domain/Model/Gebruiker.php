<?php

namespace WP3\Domain\Model;

use FOS\UserBundle\Model\User as BaseUser;

class Gebruiker extends BaseUser
{
    protected $id;

    public function __toJson()
    {
        return [
            'id' => $this->getId(),
            'username' => $this->getUsername()
        ];
    }
}
