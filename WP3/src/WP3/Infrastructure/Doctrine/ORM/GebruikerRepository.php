<?php

namespace WP3\Infrastructure\Doctrine\ORM;

use Doctrine\ORM\EntityRepository;
use WP3\Domain\Repository\GebruikerRepositoryInterface;

class GebruikerRepository extends EntityRepository implements GebruikerRepositoryInterface
{
    public function findOneById($id)
    {
        return $this->findOneBy(['id' => $id]);
    }

    public function findAllQuery()
    {
        return $this->createQueryBuilder("g");
    }

    public function findByTechnicus()
    {
        return $this->findBy(['roles' => 'ROLE_TECHNICUS']);
    }

    public function persist($gebruiker)
    {
        $this->getEntityManager()->persist($gebruiker);
        $this->getEntityManager()->flush();
    }

    public function remove($gebruiker)
    {
        $this->getEntityManager()->remove($gebruiker);
        $this->getEntityManager()->flush();
    }
}