<?php

namespace WP3\Infrastructure\Doctrine\ORM;

use Doctrine\ORM\EntityRepository;
use WP3\Domain\Repository\StatusRepositoryInterface;

class StatusRepository extends EntityRepository implements StatusRepositoryInterface
{
    public function findOneById($id)
    {
        return $this->findOneBy(['id' => $id]);
    }

    public function findAllQuery()
    {
        return $this->createQueryBuilder("s");
    }

    public function persist($status)
    {
        $this->getEntityManager()->persist($status);
        $this->getEntityManager()->flush();
    }

    public function remove($status)
    {
        $this->getEntityManager()->remove($status);
        $this->getEntityManager()->flush();
    }
}
