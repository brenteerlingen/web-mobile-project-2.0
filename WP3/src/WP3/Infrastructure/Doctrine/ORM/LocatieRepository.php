<?php

namespace WP3\Infrastructure\Doctrine\ORM;

use Doctrine\ORM\EntityRepository;
use WP3\Domain\Repository\LocatieRepositoryInterface;

class LocatieRepository extends EntityRepository implements LocatieRepositoryInterface
{
    public function findOneById($id)
    {
        return $this->findOneBy(['id' => $id]);
    }

    public function findAllQuery()
    {
        return $this->createQueryBuilder("l");
    }

    public function persist($locatie)
    {
        $this->getEntityManager()->persist($locatie);
        $this->getEntityManager()->flush();
    }

    public function remove($locatie)
    {
        $this->getEntityManager()->remove($locatie);
        $this->getEntityManager()->flush();
    }
}
