<?php

namespace WP3\Infrastructure\Doctrine\ORM;

use Doctrine\ORM\EntityRepository;
use WP3\Domain\Repository\ProbleemRepositoryInterface;

class ProbleemRepository extends EntityRepository implements ProbleemRepositoryInterface
{
    public function findOneById($id)
    {
        return $this->findOneBy(['id' => $id]);
    }

    public function findAllQuery()
    {
        return $this->createQueryBuilder("p");
    }

    public function findAllByTechnicus($technicus)
    {
//        $qb = $this->createQueryBuilder("p");
//        return $qb->select('*')
//            ->where('p.technicus = ?1')
//            ->setParameter(1, 'Technieker1')
//            ->getQuery()
//            ->getResult();
        return $this->findBy(['technicus' => $technicus]);
    }

    public function persist($probleem)
    {
        $this->getEntityManager()->persist($probleem);
        $this->getEntityManager()->flush();
    }

    public function remove($probleem)
    {
        $this->getEntityManager()->remove($probleem);
        $this->getEntityManager()->flush();
    }
}
