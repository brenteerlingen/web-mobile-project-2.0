<?php
require_once __DIR__ . '/vendor/autoload.php';

use controller\OpdrachtController;
use model\StatusModel;
use model\LocatieModel;
use model\OpdrachtModel;
use model\ProbleemMeldingModel;

use repository\PDOLocatieRepository;
use repository\PDOOpdrachtRepository;
use repository\PDOStatusRepository;
use repository\PDOProbleemMeldingRepository;
use repository\PDOTechniekerRepository;

use controller\LocatieController;
use controller\StatusController;
use controller\ProbleemMeldingController;
use controller\TechniekerController;


$user = 'root';
$password = 'root';

$database = 'WebProject';
$hostname = 'localhost';

$pdo = null;

$router = new AltoRouter();

try {
    $pdo = new PDO("mysql:host=$hostname;dbname=$database",
        $user, $password);
    $pdo->setAttribute(PDO::ATTR_ERRMODE,
        PDO::ERRMODE_EXCEPTION);

    $locatiePDORepository = new PDOLocatieRepository($pdo);
    $locatieController = new LocatieController($locatiePDORepository);
    $statusPDORepository = new PDOStatusRepository($pdo);
    $statusController = new StatusController($statusPDORepository);
    $probleemMeldingPDORepository = new PDOProbleemMeldingRepository($pdo);
    $probleemMeldingController = new ProbleemMeldingController($probleemMeldingPDORepository);
    $techniekerPDORepository = new PDOTechniekerRepository($pdo);
    $techniekerController = new TechniekerController($techniekerPDORepository);
    $opdrachtRepository = new PDOOpdrachtRepository($pdo);
    $opdrachtController = new OpdrachtController($opdrachtRepository);



    $router->setBasePath('/web-mobile-project-2.0/api/');

    $router->map('GET','locaties',
        function() use (&$locatieController) {

            header("Content-Type: application/json");
            header("Access-Control-Allow-Origin: http://localhost:3000");

            $locatieController->handleFindLocations();
        }
    );

    $router->map('GET','locaties/[i:id]',
        function($id) use (&$locatieController) {
            header("Content-Type: application/json");
            header("Access-Control-Allow-Origin: http://localhost:3000");

            $locatieController->handleFindLocationById($id);
        }
    );

    $router->map('POST','locaties/add',
        function () use (&$locatieController) {
            header("Content-Type: application/json");
            header("Access-Control-Allow-Origin: http://localhost:3000");

            $_POST = json_decode(file_get_contents('php://input'), true);
            if(isset($_POST["naam"])){
                $locatie = new LocatieModel(null, $_POST["naam"]);
                $locatieController->handleAddLocation($locatie);

            }

        }
    );

    $router->map('PUT','locaties/update/[i:id]/[:naam]',
        function($id, $naam) use (&$locatieController) {
            header("Content-Type: application/json");
            header("Access-Control-Allow-Origin: http://localhost:3000");

            $locatie = new LocatieModel($id, $naam);
            $locatieController->handleUpdateLocation($locatie);
        }
    );

    $router->map('DELETE','locaties/delete/[i:id]',
        function($id) use (&$locatieController) {
            header("Content-Type: application/json");
            header("Access-Control-Allow-Origin: http://localhost:3000");

            $locatieController->handleDeleteLocation($id);
        }
    );

    $router->map('GET','status',
        function() use (&$statusController) {
            header("Content-Type: application/json");
            header("Access-Control-Allow-Origin: http://localhost:3000");

            $statusController->handleFindAllStatussen();
        }
    );

    $router->map('GET','status/[i:id]',
        function($id) use (&$statusController) {

            header("Content-Type: application/json");
            header("Access-Control-Allow-Origin: http://localhost:3000");

            $statusController->handleFindStatusById($id);
        }
    );

    $router->map('GET', 'status/location/[i:id]',
        function($id) use (&$statusController){
            header("Content-Type: application/json");
            header("Access-Control-Allow-Origin: http://localhost:3000");

            $statusController->handleFindStatusByLocationId($id);
        }
    );

    $router->map('GET', 'status/popular',
        function() use (&$statusController){
            header("Content-Type: application/json");
            header("Access-Control-Allow-Origin: http://localhost:3000");

            $statusController->handleFindPopularStatusses();
        }
    );

    $router->map('POST', 'status/deleteold',
        function() use (&$statusController){
            header("Content-Type: application/json");
            header("Access-Control-Allow-Origin: http://localhost:3000");

            $statusController->deleteOldStatusses();
        }
    );

    $router->map('POST','status/add',
        function() use (&$statusController){

            header("Content-Type: application/json");
            header("Access-Control-Allow-Origin: http://localhost:3000");

            $_POST = json_decode(file_get_contents('php://input'), true);

            $status = new StatusModel(null,null,null,null);


                $status->setLocatieid($_POST['locatieId']);

                $status->setStatus($_POST['status']);

                $status->setDatum($_POST['datum']);

            $statusController->handleAddStatus($status);

        }
    );

    $router->map('GET', 'probleem',
        function () use (&$probleemMeldingController){
            header("Content-Type: application/json");
            header("Access-Control-Allow-Origin: http://localhost:3000");

            $probleemMeldingController->handleFindProblems();
        }
    );

    $router->map('GET', 'probleem/[i:id]',
        function ($id) use (&$probleemMeldingController){
            header("Content-Type: application/json");
            header("Access-Control-Allow-Origin: http://localhost:3000");

            $probleemMeldingController->handleFindProblemById($id);
        }
    );

    $router->map('GET', 'probleem/afgerond',
        function () use (&$probleemMeldingController){
            header("Content-Type: application/json");
            header("Access-Control-Allow-Origin: http://localhost:3000");

            $probleemMeldingController->handleFindCompletedProblems();
        }
    );

    $router->map('GET', 'probleem/afgerond/[i:id]',
        function ($id) use (&$probleemMeldingController){
            header("Content-Type: application/json");
            header("Access-Control-Allow-Origin: http://localhost:3000");

            $probleemMeldingController->handleIsCompleted($id);
        }
    );

    $router->map('POST', 'probleem/afgerond/[i:id]',
        function ($id) use (&$probleemMeldingController){
            header("Content-Type: application/json");
            header("Access-Control-Allow-Origin: http://localhost:3000");

            $probleemMeldingController->handleToggleCompleted($id);
        }
    );

    $router->map('POST', 'probleem/add',
        function () use (&$probleemMeldingController){
            header("Content-Type: application/json");
            header("Access-Control-Allow-Origin: http://localhost:3000");

            $_POST = json_decode(file_get_contents('php://input'), true);
            $probleemMelding = new ProbleemMeldingModel(null,null,null,null, null);
            if(isset($_POST['locatieId']))
                $probleemMelding->setLocatieId($_POST['locatieId']);
            if(isset($_POST['omschrijving']))
                $probleemMelding->setBeschrijving($_POST['omschrijving']);
            if(isset($_POST['datum']))
                $probleemMelding->setDatum($_POST['datum']);


            $probleemMeldingController->handleAddProblem($probleemMelding);
        }
    );

    $router->map('PUT', 'probleem/update/[i:id]',
        function ($id) use (&$probleemMeldingController){
            header("Content-Type: application/json");
            header("Acces-Control-Allow-Origin: http://localhost:3000");
            $probleemMelding = new OpdrachtModel($id,null,null,null, 0);
            if(isset($_POST['locatieId']))
                $probleemMelding->setLocatieId($_POST['locatieId']);
            if(isset($_POST['omschrijving']))
                $probleemMelding->setBeschrijving($_POST['omschrijving']);
            if(isset($_POST['datum']))
                $probleemMelding->setDatum($_POST['datum']);
            if(isset($_POST['afgehandeld']))
                $probleemMelding->setAfgerond($_POST['afgehandeld']);

            $probleemMeldingController->handleUpdateProblem($probleemMelding);
        }
    );

    $router->map('DELETE', 'probleem/delete/[i:id]',
        function ($id) use (&$probleemMeldingController){
            header("Content-Type: application/json");
            header("Access-Control-Allow-Origin: http://localhost:3000");

            $probleemMeldingController->handleDeleteProblem($id);
        }
    );

    $router->map('GET', 'technieker',
        function () use (&$techniekerController){
            header("Content-Type: application/json");
            header("Access-Control-Allow-Origin: http://localhost:3000");

            $techniekerController->handleFindTechniekers();
        }
    );

    $router->map('GET', 'technieker/[i:id]',
        function ($id) use (&$techniekerController){
            header("Content-Type: application/json");
            header("Access-Control-Allow-Origin: http://localhost:3000");

            $techniekerController->handleFindTechniekerById($id);
        }
    );


    $router->map('GET', 'opdracht',
        function () use (&$opdrachtController){
            header("Content-Type: application/json");
            header("Access-Control-Allow-Origin: http://localhost:3000");

            $opdrachtController->handleFindOpdrachten();
        }
    );

    $router->map('GET', 'opdracht/[i:id]',
        function ($id) use (&$opdrachtController){
            header("Content-Type: application/json");
            header("Access-Control-Allow-Origin: http://localhost:3000");

            $opdrachtController->handleFindOpdrachtById($id);
        }
    );

    $router->map('POST', 'opdracht/assign',
        function () use (&$opdrachtController){
            header("Content-Type: application/json");
            header("Access-Control-Allow-Origin: http://localhost:3000");

            $opdrachtController->handleAssignOpdracht($_POST['techniekerId'],$_POST['probleemMeldingId']);
        }
    );






    $match = $router->match();
    if( $match && is_callable( $match['target'] ) ) {
        call_user_func_array( $match['target'], $match['params']);
    } else {
        // no route was matched
        header( $_SERVER["SERVER_PROTOCOL"] . ' 404 Not Found');
    }

} catch (Exception $e) {
    var_dump($e);
}

