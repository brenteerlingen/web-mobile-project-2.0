<?php
/**
 * Created by PhpStorm.
 * User: jessevandenberghe
 * Date: 09/11/2017
 * Time: 18:22
 */

class TechniekerAPITest extends PHPUnit\Framework\TestCase
{
    private $http;

    public function setUp()
    {
        $this->http = new GuzzleHttp\Client(['base_uri' => '192.168.33.11/web-mobile-project-2.0/api/']);
    }

    public function tearDown()
    {
        $this->http = null;
    }

    public function testGetTechniekers()
    {
        $response = $this->http->request('GET', 'technieker');

        $this->assertEquals(200, $response->getStatusCode());

        $contentType = $response->getHeaders()["Content-Type"][0];
        $this->assertEquals("application/json", $contentType);
    }

    public function testGetTechniekerById()
    {
        $response = $this->http->request('GET', 'technieker/1');

        $this->assertEquals(200, $response->getStatusCode());

        $contentType = $response->getHeaders()["Content-Type"][0];
        $this->assertEquals("application/json", $contentType);

        $decodedJson = json_decode($response->getBody());
        $this->assertEquals($decodedJson[0]->id, "1");
    }

}