<?php

use model\ProbleemMeldingModel;
use repository\PDOProbleemMeldingRepository;

/**
 * Created by PhpStorm.
 * User: jessevandenberghe
 * Date: 06/11/2017
 * Time: 17:02
 */

class PDOProbleemMeldingRepositoryTest extends PHPUnit\Framework\TestCase
{
    public function setUp() {
        $this->mockPDO = $this->getMockBuilder('PDO')
            ->disableOriginalConstructor()
            ->getMock();
        $this->mockPDOStatement =
            $this->getMockBuilder('PDOStatement')
                ->disableOriginalConstructor()
                ->getMock();
    }

    public function testFindProblems()
    {
        $probleemmelding1 = new ProbleemMeldingModel(1, 6, "Slecht internet", "2017-29-05 00:00:00",0);
        $probleemmelding2 = new ProbleemMeldingModel(2, 1, "goed", "2017-10-09 00:00:00",1);
        $allProbleemMeldingen = array($probleemmelding1, $probleemmelding2);
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('execute');
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('fetchAll')
            ->will($this->returnValue(
                [
                    [ 'id' => $probleemmelding1->getId(),
                        'locatieId' => $probleemmelding1->getLocatieid(),
                        'beschrijving' => $probleemmelding1->getStatus(),
                        'datum' => $probleemmelding1->getDatum(),
                        'afgerond' => $probleemmelding1->getAfgerond()
                    ]
                    ,
                    [ 'id' => $probleemmelding2->getId(),
                        'beschrijving' => $probleemmelding2->getLocatieid(),
                        'status' => $probleemmelding2->getStatus(),
                        'datum' => $probleemmelding2->getDatum(),
                        'afgerond' => $probleemmelding2->getAfgerond()
                    ]
                ]));
        $this->mockPDO->expects($this->atLeastOnce())
            ->method('prepare')
            ->will($this->returnValue($this->mockPDOStatement));
        $pdoRepository = new PDOProbleemMeldingRepository($this->mockPDO);
        $actualStatus =
            $pdoRepository->findProblems();

        $this->assertEquals($allProbleemMeldingen, $actualStatus);
    }

    public function testFindProblemById()
    {
        $probleemmelding1 = new ProbleemMeldingModel(1, 6, "Slecht internet", "2017-29-05 00:00:00",0);
        $probleemmelding2 = new ProbleemMeldingModel(2, 1, "goed", "2017-10-09 00:00:00",1);

        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('bindParam');
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('execute');
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('fetchAll')
            ->will($this->returnValue(
                [
                    [ 'id' => $probleemmelding1->getId(),
                        'locatieId' => $probleemmelding1->getLocatieid(),
                        'beschrijving' => $probleemmelding1->getStatus(),
                        'datum' => $probleemmelding1->getDatum(),
                        'afgerond' => $probleemmelding1->getAfgerond()
                    ]
                ]));
        $this->mockPDO->expects($this->atLeastOnce())
            ->method('prepare')
            ->will($this->returnValue($this->mockPDOStatement));
        $pdoRepository = new PDOProbleemMeldingRepository($this->mockPDO);
        $actualStatus =
            $pdoRepository->findProblemById($probleemmelding1->getLocatieid());

        $this->assertEquals($probleemmelding1, $actualStatus);
    }

    public function testFindCompletedProblemsById()
    {
        $probleemmelding1 = new ProbleemMeldingModel(1, 6, "Slecht internet", "2017-29-05 00:00:00",0);
        $probleemmelding2 = new ProbleemMeldingModel(2, 1, "goed", "2017-10-09 00:00:00",1);
        $allProbleemMeldingen = array($probleemmelding1, $probleemmelding2);
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('execute');
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('fetchAll')
            ->will($this->returnValue(
                [
                    [ 'id' => $probleemmelding2->getId(),
                        'beschrijving' => $probleemmelding2->getLocatieid(),
                        'status' => $probleemmelding2->getStatus(),
                        'datum' => $probleemmelding2->getDatum(),
                        'afgerond' => $probleemmelding2->getAfgerond()
                    ]
                ]));
        $this->mockPDO->expects($this->atLeastOnce())
            ->method('prepare')
            ->will($this->returnValue($this->mockPDOStatement));
        $pdoRepository = new PDOProbleemMeldingRepository($this->mockPDO);
        $actualStatus =
            $pdoRepository->findCompletedProblems();

        $this->assertEquals($probleemmelding2, $actualStatus);
    }

    public function testIsCompleted()
    {
        $probleemmelding1 = new ProbleemMeldingModel(1, 6, "Slecht internet", "2017-29-05 00:00:00",1);

        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('bindParam');
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('execute');
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('fetchAll')
            ->will($this->returnValue(
                [
                    [ 'id' => $probleemmelding1->getId(),
                        'beschrijving' => $probleemmelding1->getLocatieid(),
                        'status' => $probleemmelding1->getStatus(),
                        'datum' => $probleemmelding1->getDatum(),
                        'afgerond' => $probleemmelding1->getAfgerond()
                    ]
                ]));
        $this->mockPDO->expects($this->atLeastOnce())
            ->method('prepare')
            ->will($this->returnValue($this->mockPDOStatement));
        $pdoRepository = new PDOProbleemMeldingRepository($this->mockPDO);
        $actualStatus =
            $pdoRepository->isCompleted($probleemmelding1->getLocatieid());

        $this->assertEquals($probleemmelding1->getAfgerond(), $actualStatus);
    }

    public function testToggleCompleted()
    {
        $probleemmelding1 = new ProbleemMeldingModel(1, 6, "Slecht internet", "2017-29-05 00:00:00",1);

        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('bindParam');
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('execute');
        $this->mockPDO->expects($this->atLeastOnce())
            ->method('prepare')
            ->will($this->returnValue($this->mockPDOStatement));
        $pdoRepository = new PDOProbleemMeldingRepository($this->mockPDO);
        $actualStatus =
            $pdoRepository->toggleCompleted($probleemmelding1->getLocatieid());

        $this->assertEquals("De status afgehandeld is correct omgezet", $actualStatus);
    }

    public function testAddProblem(){
        $probleemmelding1 = new ProbleemMeldingModel(1, 6, "Slecht internet", "2017-29-05 00:00:00",1);

        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('bindParam');
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('execute');
        $this->mockPDO->expects($this->atLeastOnce())
            ->method('prepare')
            ->will($this->returnValue($this->mockPDOStatement));
        $pdoRepository = new PDOProbleemMeldingRepository($this->mockPDO);
        $actualStatus =
            $pdoRepository->addProblem($probleemmelding1);

        $this->assertEquals("Probleem-melding succesvol toegevoegd.", $actualStatus);
    }

    public function testUpdateProblem(){
            $probleemmelding1 = new ProbleemMeldingModel(1, 6, "Slecht internet", "2017-29-05 00:00:00",1);

            $this->mockPDOStatement->expects($this->atLeastOnce())
                ->method('bindParam');
            $this->mockPDOStatement->expects($this->atLeastOnce())
                ->method('execute');
            $this->mockPDO->expects($this->atLeastOnce())
                ->method('prepare')
                ->will($this->returnValue($this->mockPDOStatement));
            $pdoRepository = new PDOProbleemMeldingRepository($this->mockPDO);
            $actualStatus =
                $pdoRepository->updateProblem($probleemmelding1);

            $this->assertEquals("De status is correct omgezet", $actualStatus);
    }

    public function testDeleteProblem(){
        $probleemmelding1 = new ProbleemMeldingModel(1, 6, "Slecht internet", "2017-29-05 00:00:00",1);

        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('bindParam');
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('execute');
        $this->mockPDO->expects($this->atLeastOnce())
            ->method('prepare')
            ->will($this->returnValue($this->mockPDOStatement));
        $pdoRepository = new PDOProbleemMeldingRepository($this->mockPDO);
        $actualStatus =
            $pdoRepository->addProblem($probleemmelding1->getId());

        $this->assertEquals("Probleem-melding succesvol verwijderd", $actualStatus);
    }

    public function tearDown()
    {
        $this->mockPDO = null;
        $this->mockPDOStatement = null;
    }

}