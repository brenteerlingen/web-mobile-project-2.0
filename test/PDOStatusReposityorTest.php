<?php
/**
 * Created by PhpStorm.
 * User: brenteerlingen
 * Date: 17/10/17
 * Time: 14:20
 */

use model\StatusModel;
use repository\PDOStatusRepository;

class PDOPersonRepositoryTest extends PHPUnit\Framework\TestCase
{
    public function setUp() {
        $this->mockPDO = $this->getMockBuilder('PDO')
            ->disableOriginalConstructor()
            ->getMock();
        $this->mockPDOStatement =
            $this->getMockBuilder('PDOStatement')
                ->disableOriginalConstructor()
                ->getMock();
    }

    public function testFindStatusesById_idExists_StatusObject()
    {
        $status1 = new StatusModel(1, 5, "Niet Goed", "2017-16-04 00:00:00");
        $status2 = new StatusModel(2, 1, "goed", "2017-10-09 00:00:00");
        $allStatuses = array($status1, $status2);
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('bindParam');
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('execute');
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('fetchAll')
            ->will($this->returnValue(
                [
                    [ 'id' => $status1->getId(),
                        'locatieId' => $status1->getLocatieid(),
                        'status' => $status1->getStatus(),
                        'datum' => $status1->getDatum()
                    ]
                ]));
        $this->mockPDO->expects($this->atLeastOnce())
            ->method('prepare')
            ->will($this->returnValue($this->mockPDOStatement));
        $pdoRepository = new PDOStatusRepository($this->mockPDO);
        $actualStatus =
            $pdoRepository->findStatusById($status1->getId());

        $this->assertEquals($status1, $actualStatus);
    }

    public function testFindStatusesByLocationId_idExists_StatusObject()
    {
        $status1 = new StatusModel(5, 1, "Middelmatig", "2017-11-07 00:00:00");
        $status2 = new StatusModel(9, 1, "Goed", "2017-12-03 00:00:00");
        $allStatuses = array($status1, $status2);
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('bindParam');
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('execute');
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('fetchAll')
            ->will($this->returnValue(
                [
                    [ 'id' => $status1->getId(),
                        'locatieId' => $status1->getLocatieid(),
                        'status' => $status1->getStatus(),
                        'datum' => $status1->getDatum()
                    ],
                    [ 'id' => $status2->getId(),
                        'locatieId' => $status2->getLocatieid(),
                        'status' => $status2->getStatus(),
                        'datum' => $status2->getDatum()
                    ]
                ]));
        $this->mockPDO->expects($this->atLeastOnce())
            ->method('prepare')
            ->will($this->returnValue($this->mockPDOStatement));
        $pdoRepository = new PDOStatusRepository($this->mockPDO);
        $actualStatus =
            $pdoRepository->findStatusByLocatieId($status1->getLocatieid());

        $this->assertEquals($allStatuses, $actualStatus);
    }

    public function testFindPopularStatuses_idExists_StatusObject()
    {
        $status1 = new StatusModel(11, 1, "Goed", "2017-25-07 00:00:00");
        $status2 = new StatusModel(74, 1, "Goed", "2017-21-10 00:00:00");
        $allStatuses = array($status1, $status2);
        //$this->mockPDOStatement->expects($this->atLeastOnce())
           // ->method('bindParam');
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('execute');
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('fetchAll')
            ->will($this->returnValue(
                [
                    [ 'id' => $status1->getId(),
                        'locatieId' => $status1->getLocatieid(),
                        'status' => $status1->getStatus(),
                        'datum' => $status1->getDatum()
                    ],
                    [ 'id' => $status2->getId(),
                        'locatieId' => $status2->getLocatieid(),
                        'status' => $status2->getStatus(),
                        'datum' => $status2->getDatum()
                    ]
                ]));
        $this->mockPDO->expects($this->atLeastOnce())
            ->method('prepare')
            ->will($this->returnValue($this->mockPDOStatement));
        $pdoRepository = new PDOStatusRepository($this->mockPDO);
        $actualStatus =
            $pdoRepository->findPopularLocations();

        $this->assertEquals($allStatuses, $actualStatus);
    }

    public function testFindStatusesByLocationId_idDoesNotExists_Null(){


        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('bindParam');
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('execute');
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('fetchAll')
            ->will($this->returnValue(
                []));
        $this->mockPDO->expects($this->atLeastOnce())
            ->method('prepare')
            ->will($this->returnValue($this->mockPDOStatement));
        $pdoRepository = new PDOStatusRepository($this->mockPDO);
        $actualStatus = $pdoRepository->findStatusByLocatieId(14);

        $this->assertEquals($actualStatus, '');
    }

    public function testFindAllStatuses_StatusObject(){
        $status1 = new StatusModel(5, 1, "Middelmatig", "2017-11-07 00:00:00");
        $status2 = new StatusModel(9, 1, "Goed", "2017-12-03 00:00:00");
        $allStatuses = array($status1, $status2);
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('execute');
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('fetchAll')
            ->will($this->returnValue(
                [
                    [ 'id' => $status1->getId(),
                        'locatieId' => $status1->getLocatieid(),
                        'status' => $status1->getStatus(),
                        'datum' => $status1->getDatum()
                    ],
                    [ 'id' => $status2->getId(),
                        'locatieId' => $status2->getLocatieid(),
                        'status' => $status2->getStatus(),
                        'datum' => $status2->getDatum()
                    ]
                ]));
        $this->mockPDO->expects($this->atLeastOnce())
            ->method('prepare')
            ->will($this->returnValue($this->mockPDOStatement));
        $pdoRepository = new PDOStatusRepository($this->mockPDO);
        $actualStatus =
            $pdoRepository->findAllStatusses();

        $this->assertEquals($allStatuses, $actualStatus);
    }

    public function testFindAllStatuses_Null(){
            $this->mockPDOStatement->expects($this->atLeastOnce())
                ->method('execute');
            $this->mockPDOStatement->expects($this->atLeastOnce())
                ->method('fetchAll')
                ->will($this->returnValue(
                    []));
            $this->mockPDO->expects($this->atLeastOnce())
                ->method('prepare')
                ->will($this->returnValue($this->mockPDOStatement));
            $pdoRepository = new PDOStatusRepository($this->mockPDO);
            $actualStatus = $pdoRepository->deleteStatusAfterHalfYear();

            $this->assertEquals($actualStatus, '');
    }


    public function testDeleteOldStatuses() {

        $status1 = new StatusModel(11, 1, "Goed", "2016-25-07 00:00:00");
        $status2 = new StatusModel(74, 1, "Goed", "2017-21-09 00:00:00");
        $allStatuses = array($status2);
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('execute');
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('fetchAll')
            ->will($this->returnValue(
                [

                    [ 'id' => $status2->getId(),
                        'locatieId' => $status2->getLocatieid(),
                        'status' => $status2->getStatus(),
                        'datum' => $status2->getDatum()
                    ]
                ]));
        $this->mockPDO->expects($this->atLeastOnce())
            ->method('prepare')
            ->will($this->returnValue($this->mockPDOStatement));
        $pdoRepository = new PDOStatusRepository($this->mockPDO);
        $actualStatus = $pdoRepository->deleteStatusAfterHalfYear();

        $this->assertEquals($actualStatus, $allStatuses);
    }

    public function tearDown()
    {
        $this->mockPDO = null;
        $this->mockPDOStatement = null;
    }

}