<?php
/**
 * Created by PhpStorm.
 * User: jessevandenberghe
 * Date: 09/11/2017
 * Time: 18:05
 */

class StatusAPITest extends PHPUnit\Framework\TestCase
{
    private $http;

    public function setUp()
    {
        $this->http = new GuzzleHttp\Client(['base_uri' => '192.168.33.11/web-mobile-project-2.0/api/']);
    }

    public function tearDown()
    {
        $this->http = null;
    }

    public function testGetStatussen()
    {
        $response = $this->http->request('GET', 'status');

        $this->assertEquals(200, $response->getStatusCode());

        $contentType = $response->getHeaders()["Content-Type"][0];
        $this->assertEquals("application/json", $contentType);
    }

    public function testGetStatusById()
    {
        $response = $this->http->request('GET', 'status/1');

        $this->assertEquals(200, $response->getStatusCode());

        $contentType = $response->getHeaders()["Content-Type"][0];
        $this->assertEquals("application/json", $contentType);

        $decodedJson = json_decode($response->getBody());
        $this->assertEquals($decodedJson[0]->id, "1");
    }

    public function testGetStatusByLocationId()
    {
        $response = $this->http->request('GET', 'status/1');

        $this->assertEquals(200, $response->getStatusCode());

        $contentType = $response->getHeaders()["Content-Type"][0];
        $this->assertEquals("application/json", $contentType);

        $decodedJson = json_decode($response->getBody());
        $this->assertEquals($decodedJson[0]->locatieid, "1");
    }

    public function testGetPopularStatussen()
    {
        $response = $this->http->request('GET', 'status/popular');

        $this->assertEquals(200, $response->getStatusCode());

        $contentType = $response->getHeaders()["Content-Type"][0];
        $this->assertEquals("application/json", $contentType);
    }

    public function testDeleteOldStatussen()
    {
        $response = $this->http->request('POST', 'status/deleteold');

        $this->assertEquals(200, $response->getStatusCode());

        $contentType = $response->getHeaders()["Content-Type"][0];
        $this->assertEquals("application/json", $contentType);
    }


    public function testAddStatus()
    {
        $response = $this->http->request('POST', 'status/add', [
            'form_params'=> [
                "locatieId" => "1",
                "status" => "Goed",
                "datum" => "2017-10-03"
            ]
        ]);

        $this->assertEquals(200, $response->getStatusCode());

        $contentType = $response->getHeaders()["Content-Type"][0];
        $this->assertEquals("application/json", $contentType);
    }
}