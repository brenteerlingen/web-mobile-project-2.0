<?php

use model\TechniekerModel;
use repository\PDOTechniekerRepository;

/**
 * Created by PhpStorm.
 * User: jessevandenberghe
 * Date: 06/11/2017
 * Time: 18:13
 */

class PDOTechniekerRepositoryTest extends PHPUnit\Framework\TestCase
{
    public function setUp() {
    $this->mockPDO = $this->getMockBuilder('PDO')
        ->disableOriginalConstructor()
        ->getMock();
    $this->mockPDOStatement =
        $this->getMockBuilder('PDOStatement')
            ->disableOriginalConstructor()
            ->getMock();
    }

    public function testFindTechniekers(){
        $technieker1 = new TechniekerModel(1, "Brent Eerlingen");
        $technieker2 = new TechniekerModel(2, "Jesse Van Den Berghe");
        $allTechniekers = array($technieker1, $technieker2);
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('execute');
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('fetchAll')
            ->will($this->returnValue(
                [
                    [
                        'id' => $technieker1->getId(),
                        'naam' => $technieker1->getNaam()
                    ]
                    ,
                    [
                        'id' => $technieker2->getId(),
                        'naam' => $technieker2->getNaam()
                    ]
                ]));
        $this->mockPDO->expects($this->atLeastOnce())
            ->method('prepare')
            ->will($this->returnValue($this->mockPDOStatement));
        $pdoRepository = new PDOTechniekerRepository($this->mockPDO);
        $actualStatus =
            $pdoRepository->findTechniekers();

        $this->assertEquals($allTechniekers, $actualStatus);
    }

    public function testFindTechniekerById(){
        $technieker1 = new TechniekerModel(1, "Brent Eerlingen");
        $technieker2 = new TechniekerModel(2, "Jesse Van Den Berghe");
        $allTechniekers = array($technieker1, $technieker2);
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('bindParam');
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('execute');
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('fetchAll')
            ->will($this->returnValue(
                [
                    [
                        'id' => $technieker1->getId(),
                        'naam' => $technieker1->getNaam()
                    ]
                ]));
        $this->mockPDO->expects($this->atLeastOnce())
            ->method('prepare')
            ->will($this->returnValue($this->mockPDOStatement));
        $pdoRepository = new PDOTechniekerRepository($this->mockPDO);
        $actualStatus =
            $pdoRepository->findTechniekerById($technieker1->getId());

        $this->assertEquals($technieker1, $actualStatus);
    }

    public function tearDown()
    {
        $this->mockPDO = null;
        $this->mockPDOStatement = null;
    }
}