<?php

use model\LocatieModel;
use repository\PDOLocatieRepository;

class PDOPersonRepositoryTest extends PHPUnit\Framework\TestCase
{
    public function setUp() {
        $this->mockPDO = $this->getMockBuilder('PDO')
            ->disableOriginalConstructor()
            ->getMock();
        $this->mockPDOStatement =
            $this->getMockBuilder('PDOStatement')
                ->disableOriginalConstructor()
                ->getMock();
    }

    public function testFindLocationsById_idExists_LocationObject()
    {
        $location1 = new LocatieModel(1, 'PXL D Blok');
        $location2 = new LocatieModel(2,'PXL B Blok');
        $allLocations = array($location1, $location2);
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('bindParam');
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('execute');
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('fetchAll')
            ->will($this->returnValue(
                [
                    [ 'id' => $location1->getId(),
                        'naam' => $location1->getNaam()
                    ]
                ]));
        $this->mockPDO->expects($this->atLeastOnce())
            ->method('prepare')
            ->will($this->returnValue($this->mockPDOStatement));
        $pdoRepository = new PDOLocatieRepository($this->mockPDO);
        $actualLocation =
            $pdoRepository->findLocationById($location1->getId());

        $this->assertEquals($location1, $actualLocation);
    }

    public function tearDown()
    {
        $this->mockPDO = null;
        $this->mockPDOStatement = null;
    }

}