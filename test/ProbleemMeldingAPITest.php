<?php
/**
 * Created by PhpStorm.
 * User: jessevandenberghe
 * Date: 09/11/2017
 * Time: 17:49
 */

class ProbleemMeldingAPITest extends PHPUnit\Framework\TestCase
{
    private $http;

    public function setUp()
    {
        $this->http = new GuzzleHttp\Client(['base_uri' => '192.168.33.11/web-mobile-project-2.0/api/']);
    }

    public function tearDown()
    {
        $this->http = null;
    }

    public function testGetProbleemMeldingen()
    {
        $response = $this->http->request('GET', 'probleem');

        $this->assertEquals(200, $response->getStatusCode());

        $contentType = $response->getHeaders()["Content-Type"][0];
        $this->assertEquals("application/json", $contentType);
    }

    public function testGetProbleemMeldingById()
    {
        $response = $this->http->request('GET', 'probleem/1');

        $this->assertEquals(200, $response->getStatusCode());

        $contentType = $response->getHeaders()["Content-Type"][0];
        $this->assertEquals("application/json", $contentType);

        $decodedJson = json_decode($response->getBody());
        $this->assertEquals($decodedJson[0]->id, "1");
    }

    public function testGetAfgerondeProbleemMeldingen()
    {
        $response = $this->http->request('GET', 'probleem/afgerond');

        $this->assertEquals(200, $response->getStatusCode());

        $contentType = $response->getHeaders()["Content-Type"][0];
        $this->assertEquals("application/json", $contentType);
    }

    public function testGetAfgerondeProbleemMeldingById()
    {
        $response = $this->http->request('GET', 'probleem/afgerond/1');

        $this->assertEquals(200, $response->getStatusCode());

        $contentType = $response->getHeaders()["Content-Type"][0];
        $this->assertEquals("application/json", $contentType);
    }

    public function testToggleAfgerondeProbleemMeldingById()
    {
        $response = $this->http->request('POST', 'probleem/afgerond/1');

        $this->assertEquals(200, $response->getStatusCode());

        $contentType = $response->getHeaders()["Content-Type"][0];
        $this->assertEquals("application/json", $contentType);
    }

    public function testAddProbleemMelding(){
        $response = $this->http->request('POST', 'probleem/add', [
            'form_params'=> [
                "locatieId" => "1",
                "omschrijving" => "Goed",
                "datum" => "2017-10-03",
                "afgehandeld" => "1"
            ]
        ]);

        $this->assertEquals(200, $response->getStatusCode());

        $contentType = $response->getHeaders()["Content-Type"][0];
        $this->assertEquals("application/json", $contentType);
    }

    public function testUpdateProbleemMelding(){
        $response = $this->http->request('PUT', 'probleem/update/1', [
            'form_params'=> [
                "locatieId" => "1",
                "omschrijving" => "Goed",
                "datum" => "2017-10-03",
                "afgehandeld" => "1"
            ]
        ]);

        $this->assertEquals(200, $response->getStatusCode());

        $contentType = $response->getHeaders()["Content-Type"][0];
        $this->assertEquals("application/json", $contentType);
    }

    public function testDeleteLocation(){
        $response = $this->http->request('DELETE', 'probleem/delete/1');

        $this->assertEquals(200, $response->getStatusCode());

        $contentType = $response->getHeaders()["Content-Type"][0];
        $this->assertEquals("application/json", $contentType);
    }


}