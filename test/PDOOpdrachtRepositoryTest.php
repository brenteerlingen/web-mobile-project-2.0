<?php

use model\OpdrachtModel;
use model\TechniekerModel;
use repository\PDOOpdrachtRepository;
use repository\PDOTechniekerRepository;

/**
 * Created by PhpStorm.
 * User: jessevandenberghe
 * Date: 06/11/2017
 * Time: 18:13
 */

class PDOOpdrachtRepositoryTest extends PHPUnit\Framework\TestCase
{
    public function setUp() {
    $this->mockPDO = $this->getMockBuilder('PDO')
        ->disableOriginalConstructor()
        ->getMock();
    $this->mockPDOStatement =
        $this->getMockBuilder('PDOStatement')
            ->disableOriginalConstructor()
            ->getMock();
    }

    public function testFindOpdrachten(){
        $opdracht1 = new OpdrachtModel(1, 1,1);
        $opdracht2 = new OpdrachtModel(2, 2,2);
        $allOpdrachten = array($opdracht1, $opdracht2);
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('execute');
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('fetchAll')
            ->will($this->returnValue(
                [
                    [
                        'id' => $opdracht1->getId(),
                        'techniekerId' => $opdracht1->getProbleemMeldingId(),
                        'probleemMeldingId' => $opdracht1->getProbleemMeldingId()
                    ]
                    ,
                    [
                        'id' => $opdracht2->getId(),
                        'techniekerId' => $opdracht2->getProbleemMeldingId(),
                        'probleemMeldingId' => $opdracht2->getProbleemMeldingId()
                    ]
                ]));
        $this->mockPDO->expects($this->atLeastOnce())
            ->method('prepare')
            ->will($this->returnValue($this->mockPDOStatement));
        $pdoRepository = new PDOOpdrachtRepository($this->mockPDO);
        $actualStatus =
            $pdoRepository->findOpdrachten();

        $this->assertEquals($allOpdrachten, $actualStatus);
    }

    public function testFindOpdrachtById(){
        $opdracht1 = new OpdrachtModel(1, 1,1);
        $opdracht2 = new OpdrachtModel(2, 2,2);
        $allOpdrachten = array($opdracht1, $opdracht2);
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('execute');
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('fetchAll')
            ->will($this->returnValue(
                [
                    [
                        'id' => $opdracht1->getId(),
                        'techniekerId' => $opdracht1->getProbleemMeldingId(),
                        'probleemMeldingId' => $opdracht1->getProbleemMeldingId()
                    ]
                ]));
        $this->mockPDO->expects($this->atLeastOnce())
            ->method('prepare')
            ->will($this->returnValue($this->mockPDOStatement));
        $pdoRepository = new PDOOpdrachtRepository($this->mockPDO);
        $actualStatus =
            $pdoRepository->findOpdrachtById($opdracht1->getId());

        $this->assertEquals($opdracht1, $actualStatus);
    }

    public function testAssignOpdracht(){
        $opdracht1 = new OpdrachtModel(1, 1,1);

        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('bindParam');
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('execute');
        $this->mockPDO->expects($this->atLeastOnce())
            ->method('prepare')
            ->will($this->returnValue($this->mockPDOStatement));
        $pdoRepository = new PDOOpdrachtRepository($this->mockPDO);
        $actualStatus =
            $pdoRepository->assignOpdracht($opdracht1->getTechniekerId(),$opdracht1->getProbleemMeldingId());

        $this->assertEquals("Opdracht succesvol toegevoegd.", $actualStatus);
    }

    public function tearDown()
    {
        $this->mockPDO = null;
        $this->mockPDOStatement = null;
    }
}