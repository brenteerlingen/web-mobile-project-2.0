<?php
/**
 * Created by PhpStorm.
 * User: jessevandenberghe
 * Date: 09/11/2017
 * Time: 17:49
 */

class LocatieAPITest extends PHPUnit\Framework\TestCase
{
    private $http;

    public function setUp()
    {
        $this->http = new GuzzleHttp\Client(['base_uri' => '192.168.33.11/web-mobile-project-2.0/api/']);
    }

    public function tearDown()
    {
        $this->http = null;
    }

    public function testGetLocations()
    {
        $response = $this->http->request('GET', 'locaties');

        $this->assertEquals(200, $response->getStatusCode());

        $contentType = $response->getHeaders()["Content-Type"][0];
        $this->assertEquals("application/json", $contentType);
    }

    public function testGetLocationById()
    {
        $response = $this->http->request('GET', 'locaties/1');

        $this->assertEquals(200, $response->getStatusCode());

        $contentType = $response->getHeaders()["Content-Type"][0];
        $this->assertEquals("application/json", $contentType);

        $decodedJson = json_decode($response->getBody());
        $this->assertEquals($decodedJson[0]->id, "1");
    }

    public function testAddLocation(){
        $response = $this->http->request('POST', 'locaties/add', [
            'form_params'=> [
                "naam" => "PXL"
            ]
        ]);

        $this->assertEquals(200, $response->getStatusCode());

        $contentType = $response->getHeaders()["Content-Type"][0];
        $this->assertEquals("application/json", $contentType);
    }

    public function testUpdateLocation(){
        $response = $this->http->request('PUT', 'locaties/update/1/PXL', [
            'form_params'=> [
                "naam" => "PXL"
            ]
        ]);

        $this->assertEquals(200, $response->getStatusCode());

        $contentType = $response->getHeaders()["Content-Type"][0];
        $this->assertEquals("application/json", $contentType);
    }

    public function testDeleteLocation(){
        $response = $this->http->request('DELETE', 'locaties/delete/1');

        $this->assertEquals(200, $response->getStatusCode());

        $contentType = $response->getHeaders()["Content-Type"][0];
        $this->assertEquals("application/json", $contentType);
    }


}